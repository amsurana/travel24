$(function() {
    showLoader()
    var login_status = localStorage.getItem("login_status");
    if (login_status == 1) {
        $.ajax({
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            url: window.location.origin + "/travel24/checkpost/all",
        }).done(function(response) {
            for (var i = 0; i < response.resp.length; i++) {
                $("<option value=" + response.resp[i].id + ">" + response.resp[i].name + "</option>").appendTo($("#cp_list_from,#cp_list_to"));
            }

        });

        $.ajax({
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            url: window.location.origin + "/travel24/master/all",
        }).done(function(response) {
            hideLoader();
            loadTripsDatatable(response.resp);
        });

    } else {
        window.location.replace(window.location.origin + "/travel24/");
    }

    createTrip();
})

function loadTripsDatatable(dataSet) {
    if (typeof dataTable_trips == 'undefined') {
        dataTable_trips = $("#tripsTable")
            .dataTable({
                "bDestroy": true,
                "bRetrieve": true,
                "bProcessing": true,
                "bDeferRender": true,
                "aaData": dataSet,
                "aoColumns": [{
                    "sTitle": "From",
                    "mData": "fromCheckPost",
                    "mRender": function(data, type, row) {
                        return data.name;
                    }
                }, {
                    "sTitle": "To",
                    "mData": "toCheckPost",
                    "mRender": function(data, type, row) {
                        return data.name;
                    }
                }, {
                    "sTitle": "Distance",
                    "mData": "distance"
                }, {
                    "sTitle": "Time",
                    "mData": "timeTaken"
                }, {
                    "sTitle": "Action",
                    "mData": "data",
                    "mRender": function(datam, type, row) {
                        return "&nbsp <button class='btn btn-default' id='deleteInfo'><i class='fa fa-trash' aria-hidden='true '></i></button>"
                    }
                }]

            })

    } else {
        dataTable_trips.dataTable().fnClearTable();
        if (dataSet.length != 0) {

            dataTable_trips.fnAddData(dataSet)

        }
    }
}


$("#tripsTable").on('click', '#deleteInfo', function(event) {
    showLoader();
    var row = $(this).closest("tr").get(0);
    var rowIndex = dataTable_trips.fnGetPosition(row);
    var rowData = dataTable_trips.fnGetData(rowIndex);
    var id = rowData.id;

    $.ajax({
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        url: "master/delete?id=" + id
    }).done(function(response) {
        hideLoader();
        alert("successfully deleted!");
        location.reload();
    })

});

function createTrip() {
    showLoader()
    $("#create_trip_form").validate({
        rules: {
            cp_list_from: {
                required: true
            },
            cp_list_to: {
                required: true
            },
            trip_duration: {
                required: true
            },
            trip_distance: {
                required: true
            }

        },
        submitHandler: function(form) {
            var fromCp = $("#cp_list_from").val();
            var toCp = $("#cp_list_to").val();
            var timeTaken = $("#trip_duration").val();
            var distance = $("#trip_distance").val();

            if (toCp == "" || timeTaken == "" || distance == "") {
                alert("Fill all required fields");
            } else {
                var data = {
                    "request": {
                        "fromCp": fromCp,
                        "toCp": toCp,
                        "timeTaken": timeTaken,
                        "distance": distance
                    }
                }
                $.ajax({
                    type: "POST",
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    url: window.location.origin + "/travel24/master/add",
                    data: JSON.stringify(data)
                }).done(function(response) {
                    hideLoader();
                    alert("Successfully Added")
                    $("#create_trip_form")[0].reset();
                    location.reload();
                })
                return false;
            }
        }


    });

}
