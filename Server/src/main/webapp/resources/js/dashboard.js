$(function() {
    showLoader();
    var login_status = localStorage.getItem("login_status");
    if (login_status == 1) {
        $.ajax({
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            url: "trip/all",
        }).done(function(response) {
            hideLoader()
            managingTripsDatatable(response.resp);
            loadCharts(response.resp);
        });

        $.ajax({
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            url: window.location.origin + "/travel24/checkpost/all",
        }).done(function(response) {
            for (var i = 0; i < response.resp.length; i++) {
                $("<option value=" + response.resp[i].id + ">" + response.resp[i].name + "</option>").appendTo($("#cp_list_to,#cp_list_from"));
            }

        });

        $('#fromDate').datetimepicker({

            timepicker: true,
            defaultTime: '00:00',
            showOn: "button",
            format: "d-m-Y H:i",
            maxDate: 0,
            onChangeDateTime: function(current_time, $input) {
                $("#fromDate").data("Date", current_time.getTime());

            }

        });
        $('#toDate').datetimepicker({

            timepicker: true,
            defaultTime: '00:00',
            showOn: "button",
            format: "d-m-Y H:i",
            maxDate: 0,
            onChangeDateTime: function(current_time, $input) {
                $("#toDate").data("Date", current_time.getTime());

            }

        });

    } else {
        window.location.replace(window.location.origin + "/travel24/");
    }

    searchTrip();


})

function managingTripsDatatable(dataSet) {
    if (typeof dataTable_managetrips == 'undefined') {
        dataTable_managetrips = $("#manageTrips")
            .dataTable({
            	dom: 'Bfrtip',
            	buttons: [
            	            {
            	                extend: 'csvHtml5',
            	                title: 'Travel24_Data'
            	            },
            	            {
            	                extend: 'copyHtml5',
            	            },
            	        ],
                pageLength:25,
                lengthMenu: [25, 50, 75, "All"],
                "destroy": true,
                "retrieve": true,
                "processing": true,
                "deferRender": true,
                "data": dataSet,
                "columns": [{
                    "title": "Vehicle Type",
                    "data": "vehicleDetails",
                    "render": function(datam, type, row) {
                        return datam.type
                    }

                }, {
                    "title": "Vehicle No.",
                    "data": "vehicleDetails",
                    "render": function(datam, type, row) {
                        if (datam == 0) {
                            return "NA"
                        }
                        return datam.number
                    }

                }, {
                    "title": "Driver Name",
                    "data": "driverName",
                    "render": function(datam, type, row) {
                        if (datam == 0) {
                            return "NA"
                        }
                        return datam
                    }

                }, {
                    "title": "Start Cp",
                    "data": "fromCheckPost",
                    "render": function(datam, type, row) {
                        return datam.name
                    }

                }, {
                    "title": "End Cp",
                    "data": "endCheckPost",
                    "render": function(datam, type, row) {
                        return datam.name
                    }
                }, {
                    "title": "Entry Time",
                    "data": "entryTime",
                    "render": function(datam, type, row) {
                        if (datam == 0) {
                            return "NA";
                        }
                        return new Date(datam).customFormat("#DD#-#MM#-#YYYY# #hh#:#mm# #AMPM#");
                    }

                }, {
                    "title": "Exit Time",
                    "data": "exitTime",
                    "render": function(datam, type, row) {
                        if (datam == 0) {
                            return "NA";
                        }
                        return new Date(datam).customFormat("#DD#-#MM#-#YYYY# #hh#:#mm# #AMPM#");
                    }

                }, {
                    "title": "Delay",
                    "data": "delay",
                    "render": function(datam, type, row) {
                        if (datam == 1) {
                            return "Yes"
                        } else {
                            return "No"
                        }
                    }
                }, {
                    "title": "Over",
                    "data": "over",
                    "render": function(datam, type, row) {
                        if (datam == 1) {
                            return "Yes"
                        } else {
                            return "No"
                        }
                    }

                }, {
                    "title": "No. of Passengers",
                    "data": "numberOfPassenger"
                }, {
                    "title": "Remark",
                    "data": "remark",
                    "render": function(datam, type, row) {
                        if (datam == 0) {
                            return "NA"
                        }
                        return datam
                    }



                }]
            })

    } else {
        dataTable_managetrips.dataTable().fnClearTable();
        if (dataSet.length != 0) {

            dataTable_managetrips.fnAddData(dataSet)

        }
    }
}

function searchTrip() {
    showLoader()
    $("#search_trips_form").validate({
        rules: {},
        submitHandler: function(form) {
            var fromDate
            var fromCp
            var toDate
            var toCp
            $("#fromDate").data().Date != undefined ?
                fromDate = $("#fromDate").data().Date : fromDate = 0;

            $("#toDate").data().Date != undefined ?
                toDate = $("#toDate").data().Date : toDate = 0;

            $("#cp_list_from").val() != 0 ?
                fromCp = $("#cp_list_from").val() : fromCp = 0;

            $("#cp_list_to").val() != 0 ?
                toCp = $("#cp_list_to").val() : toCp = 0;

            var data = {
                "request": {
                    "fromDate": fromDate,
                    "toDate": toDate,
                    "fromCp": fromCp,
                    "toCp": toCp,
                }
            }
            $.ajax({
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                url: "trip/search",
                data: JSON.stringify(data)
            }).done(function(response) {
                hideLoader();
                managingTripsDatatable(response.resp);
                loadCharts(response.resp);

            })
            return false;
        }
    });

}
var barChartOptions = {
    //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
    scaleBeginAtZero: true,
    //Boolean - Whether grid lines are shown across the chart
    scaleShowGridLines: true,
    //String - Colour of the grid lines
    scaleGridLineColor: "rgba(0,0,0,.05)",
    //Number - Width of the grid lines
    scaleGridLineWidth: 1,
    //Boolean - Whether to show horizontal lines (except X axis)
    scaleShowHorizontalLines: true,
    //Boolean - Whether to show vertical lines (except Y axis)
    scaleShowVerticalLines: true,
    //Boolean - If there is a stroke on each bar
    barShowStroke: true,
    //Number - Pixel width of the bar stroke
    barStrokeWidth: 1,
    //Number - Spacing between each of the X value sets
    barValueSpacing: 5,
    //Number - Spacing between data sets within X values
    barDatasetSpacing: 1,
    //String - A legend template
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
    //Boolean - whether to make the chart responsive
    responsive: true,
    maintainAspectRatio: false

};


function loadCharts(chartData) {



    var over_speed_vehicle = [];
    var num_over_speed_vehicle = [];
    var delay_vehicle = [];
    var num_delay_vehicle = [];
    var overSpeed = {};
    var delay = {};

    for (i = 0; i < chartData.length; i++) {

        if (chartData[i].over == 1) {

            var type = chartData[i].vehicleDetails.type;
            overSpeed[type] = (overSpeed[type] ? overSpeed[type] : 0) + 1;
        }
        if (chartData[i].delay == 1) {
            var type = chartData[i].vehicleDetails.type;
            delay[type] = (delay[type] ? delay[type] : 0) + 1;

        }

    }

    $.each(overSpeed, function(key, value) {
        over_speed_vehicle.push(key);
        num_over_speed_vehicle.push(value);
    });

    $.each(delay, function(key, value) {
        delay_vehicle.push(key);
        num_delay_vehicle.push(value);
    });


    var overSpeedChartCanvas = $("#overSpeedChart").get(0).getContext("2d");
    var overSpeedChart = new Chart(overSpeedChartCanvas);

    var overSpeedChartData = {
        labels: over_speed_vehicle,
        datasets: [{
            label: "over speed",
            fillColor: "rgba(210, 214, 222, 1)",
            strokeColor: "rgba(210, 214, 222, 1)",
            pointColor: "rgba(210, 214, 222, 1)",
            pointStrokeColor: "#c1c7d1",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
            data: num_over_speed_vehicle
        }]
    };
    overSpeedChartData.datasets[0].fillColor = "rgba(10,160,87,.45)";
    overSpeedChartData.datasets[0].strokeColor = "rgba(10,160,87,.45)";
    overSpeedChartData.datasets[0].pointColor = "rgba(10,160,87,.45)";
    barChartOptions.datasetFill = false;

    var delayChartCanvas = $("#delayCharts").get(0).getContext("2d");
    var delayChart = new Chart(delayChartCanvas);

    var delayChartData = {
        labels: delay_vehicle,
        datasets: [{
            label: "over speed",
            fillColor: "rgba(210, 214, 222, 1)",
            strokeColor: "rgba(210, 214, 222, 1)",
            pointColor: "rgba(210, 214, 222, 1)",
            pointStrokeColor: "#c1c7d1",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
            data: num_delay_vehicle
        }]
    };
    delayChartData.datasets[0].fillColor = "rgba(41,139,142,.57)";
    delayChartData.datasets[0].strokeColor = "rgba(41,139,142,.57)";
    delayChartData.datasets[0].pointColor = "rgba(41,139,142,.57)";
    barChartOptions.datasetFill = false;

    overSpeedChart.Bar(overSpeedChartData, barChartOptions);
    delayChart.Bar(delayChartData, barChartOptions);

}
