$(function() {
    showLoader();
    var login_status = localStorage.getItem("login_status");
    if (login_status == 1) {
        $.ajax({
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            url: window.location.origin + "/travel24/checkpost/all",
        }).done(function(response) {
            hideLoader();

            loadCheckPointDatatable(response.resp);
        });


    } else {
        window.location.replace(window.location.origin + "/travel24/");
    }
    createCheckPoint();
    updateCheckpoint();

});

function loadCheckPointDatatable(dataSet) {
    if (typeof dataTable_checkpoint == 'undefined') {
        dataTable_checkpoint = $("#checkpostTable")
            .dataTable({
                "bDestroy": true,
                "bRetrieve": true,
                "bProcessing": true,
                "bDeferRender": true,
                "aaData": dataSet,
                "aoColumns": [{
                    "sTitle": "Id",
                    "mData": "id"

                }, {
                    "sTitle": "Name",
                    "mData": "name"
                }, {
                    "sTitle": "Action",
                    "mData": "data",
                    "mRender": function(datam, type, row) {
                        return "<button class='btn btn-default' data-toggle='modal'  id='editInfo'><i class='fa fa-pencil' aria-hidden='true '></i></button> &nbsp <button class='btn btn-default' id='deleteInfo'><i class='fa fa-trash' aria-hidden='true '></i></button>"
                    }
                }]
            })

    } else {
        dataTable_checkpoint.dataTable().fnClearTable();
        if (dataSet.length != 0) {

            dataTable_checkpoint.fnAddData(dataSet)

        }
    }
}

function createCheckPoint() {
    showLoader();
    $("#create_cp_form").validate({
        rules: {
            cp_name: {
                required: true
            }

        },
        submitHandler: function(form) {
            //showLoader();
            // Defined in utility.js

            var cp_name = $("#cp_name").val();
            var data = {
                "request": {
                    "name": cp_name
                }
            }
            $.ajax({
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                url: window.location.origin + "/travel24/checkpost/add",
                data: JSON.stringify(data)
            }).done(function(response) {
                hideLoader();
                $("#create_cp_form")[0].reset();
                location.reload();
            })
            return false;
        }
    });
}

$("#checkpostTable").on('click', '#editInfo', function(event) {
    $("#editPopup").modal("show");
    var row = $(this).closest("tr").get(0);
    var rowIndex = dataTable_checkpoint.fnGetPosition(row);
    var rowData = dataTable_checkpoint.fnGetData(rowIndex);
    var id = rowData.id;
    $("#checkpostId").val(id);

});

$("#checkpostTable").on('click', '#deleteInfo', function(event) {
    showLoader();
    var row = $(this).closest("tr").get(0);
    var rowIndex = dataTable_checkpoint.fnGetPosition(row);
    var rowData = dataTable_checkpoint.fnGetData(rowIndex);
    var id = rowData.id;

    $.ajax({
        type: "GET",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        url: "checkpost/delete?id=" + id,
    }).done(function(response) {
        hideLoader();
        alert("successfully deleted!");
        location.reload();
    })

});

function updateCheckpoint() {
    showLoader();
    $("#editPopupForm").validate({
        rules: {

        },
        submitHandler: function(form) {
            var cp_name = $("#changeName").val();
            var id = $("#checkpostId").val();
            var data = {
                "request": {
                    "id": id,
                    "name": cp_name
                }
            }

            $.ajax({
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: JSON.stringify(data),
                url: "checkpost/update"
            }).done(function(response) {
                hideLoader();
                alert("successfully updated!");
                location.reload();
            })

            return false;
        }
    });
}
