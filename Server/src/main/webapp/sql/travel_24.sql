-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 31, 2016 at 01:41 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.5.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `travel24`
--

-- --------------------------------------------------------

--
-- Table structure for table `check_posts`
--

CREATE TABLE `check_posts` (
  `id` int(30) NOT NULL,
  `name` varchar(15) NOT NULL,
  `created_time` bigint(10) NOT NULL,
  `updated_time` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `check_posts`
--

INSERT INTO `check_posts` (`id`, `name`, `created_time`, `updated_time`) VALUES
(1, 'spt', 1472128218999, 0),
(2, 'tirupathi', 1472128301427, 0),
(3, 'kadapa', 1472205330205, 0),
(4, 'kurnool', 1472209973788, 0),
(5, 'chithoor', 1472554920482, 0);

-- --------------------------------------------------------

--
-- Table structure for table `master_details`
--

CREATE TABLE `master_details` (
  `id` int(10) NOT NULL,
  `from_cp` int(10) NOT NULL,
  `to_cp` int(10) NOT NULL,
  `time_taken` int(10) NOT NULL,
  `distance` int(10) NOT NULL,
  `created_time` bigint(10) NOT NULL,
  `updated_time` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master_details`
--

INSERT INTO `master_details` (`id`, `from_cp`, `to_cp`, `time_taken`, `distance`, `created_time`, `updated_time`) VALUES
(1, 1, 2, 20, 30, 1472202992676, 0),
(2, 1, 2, 30, 50, 1472205122483, 0),
(3, 1, 2, 40, 60, 1472205598688, 0),
(4, 1, 2, 50, 90, 1472556503957, 0);

-- --------------------------------------------------------

--
-- Table structure for table `trip_details`
--

CREATE TABLE `trip_details` (
  `id` int(20) NOT NULL,
  `vehicles_id` int(11) DEFAULT NULL,
  `driver_name` varchar(20) DEFAULT NULL,
  `number_of_passenger` int(50) DEFAULT NULL,
  `start_cp` int(10) DEFAULT NULL,
  `end_cp` int(10) DEFAULT NULL,
  `entry_time` bigint(30) DEFAULT NULL,
  `exit_time` bigint(30) DEFAULT NULL,
  `Over` tinyint(1) DEFAULT NULL,
  `Delay` tinyint(1) DEFAULT NULL,
  `Remark` varchar(20) DEFAULT NULL,
  `created_time` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trip_details`
--

INSERT INTO `trip_details` (`id`, `vehicles_id`, `driver_name`, `number_of_passenger`, `start_cp`, `end_cp`, `entry_time`, `exit_time`, `Over`, `Delay`, `Remark`, `created_time`) VALUES
(53, 13, 'manoj', 60, 2, 1, 5021, 0, 1, 1, 'plss', 1472633681297),
(54, 14, 'manup', 80, 2, 1, 7923, 0, 1, 1, 'pllsdk', 1472633765680),
(55, 14, 'manu', 80, 3, 1, 7923, 0, 1, 1, 'pllsdk', 1472633840468);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL,
  `cp_details` int(10) NOT NULL,
  `created_time` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `cp_details`, `created_time`) VALUES
(1, 'bindu', 'admin', 1, 1472130112791),
(2, 'satish', 'admin', 2, 1472132106147),
(3, 'santhosh', 'admin', 2, 1472189880750),
(6, 'girish', 'admin', 2, 1472551436421),
(7, 'sravanthi', 'admin', 3, 1472554590794);

-- --------------------------------------------------------

--
-- Table structure for table `vehicles`
--

CREATE TABLE `vehicles` (
  `id` int(20) NOT NULL,
  `type` varchar(50) NOT NULL,
  `number` varchar(50) NOT NULL,
  `created_time` bigint(10) NOT NULL,
  `updated_time` bigint(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehicles`
--

INSERT INTO `vehicles` (`id`, `type`, `number`, `created_time`, `updated_time`) VALUES
(11, 'bus', 'AP024', 1472622220352, NULL),
(12, 'car', 'AP089', 1472624939769, NULL),
(13, 'lorry', 'AP074', 1472633681124, NULL),
(14, 'bus', 'AP063', 1472633765609, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `check_posts`
--
ALTER TABLE `check_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_details`
--
ALTER TABLE `master_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trip_details`
--
ALTER TABLE `trip_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cp_details` (`cp_details`);

--
-- Indexes for table `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `check_posts`
--
ALTER TABLE `check_posts`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `master_details`
--
ALTER TABLE `master_details`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `trip_details`
--
ALTER TABLE `trip_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `vehicles`
--
ALTER TABLE `vehicles`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`cp_details`) REFERENCES `check_posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;