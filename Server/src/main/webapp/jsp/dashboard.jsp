<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Travel24</title>
</head>
<body>
     <%@ include file="header.jsp" %>
     <%@ include file="side-menu.jsp" %>
    <div class="content-wrapper" >
      <section class="content">
          <div class="row" style="margin:0px !important;">
            <div class="box  panel box box-warning" style="box-shadow: 0px 2px 10px #868282;">
                <div class="box-header" data-widget="collapse-header">
                    <h3 class="box-title">Search</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <form id="search_trips_form" name="search_trips_form">
                    <div class="form-group col-md-3">
                      <label for="cp_list_from">From Date:</label>
                      <div class='input-group Date optional'>
                          <input type='text' id='fromDate' placeholder="Enter From Date"
                             class="form-control" />
                          <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                          </span>
                       </div>
                    </div>

                    <div class="form-group col-md-3">
                      <label for="cp_list-to">To Date:</label>
                        <div class='input-group Date optional'>
                          <input type='text' id='toDate' placeholder="Enter To Date"
                             class="form-control" />
                          <span class="input-group-addon">
                          <span class="glyphicon glyphicon-calendar"></span>
                          </span>
                       </div>
                    </div>


                    <div class="form-group col-md-3">
                      <label for="trip_distance">From Checkpost:</label>
                      <select class="form-control" id="cp_list_from" >
                          <option value="">--Select Checkpoint--</option>
                      </select>
                    </div>

                    <div class="form-group col-md-3">
                      <label for="trip_duration">To CheckPost:</label>
                      <select class="form-control" id="cp_list_to"  >
                            <option value="">--Select Checkpoint--</option>
                        </select>
                    </div>

                    <div class="form-group float-right" style="margin-right: 14px;">
                      <input type="submit" class="btn btn-warning" value="search" />
                    </div>

                  </form>
                </div>
              </div><!-- /.box -->
          </div>
          <div class="row" style="margin:0px !important;">
            <div class="box  panel box box-warning " style="box-shadow: 0px 2px 10px #868282;">
            <div class="box-header with-border">
              <h3 class="box-title">Bar Chart</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="col-md-12">
                  <div class="col-md-6">
                      <h5 style="margin-left:42%;"  >overspeed  &nbsp <i class="fa fa-bar-chart" aria-hidden="true"></i></h5>
                     <div class="chart">
                        <canvas id="overSpeedChart" style="height: 230px; width: 716px;" width="716" height="230"></canvas>
                     </div>
                  </div>
                  <div class="col-md-6">
                      <h5  style="margin-left:42%;" >delay   &nbsp <i class="fa fa-bar-chart" aria-hidden="true"></i></h5>
                         <div class="chart">
                            <canvas id="delayCharts" style="height: 230px; width: 716px;" width="716" height="230"></canvas>
                         </div>
                        <!-- /.box-body -->
                  </div>
              </div>

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          </div>
          <div class="row">
           <div class="col-md-12">
              <div class="height10"></div>
              <div class=" tableSection sectionBgWhite">
                 <div class="height20"></div>
                 <table id="manageTrips" class="stripe table table-bordered table-striped" width="100%">
                 </table>
              </div>
           </div>
        </div>
      </section>
    </div>
</body>
</html>
<script type="text/javascript"
  src="<c:url value="/resources/js/dashboard.js" />"></script>