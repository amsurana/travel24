<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"%>

<aside class="main-sidebar">


  <section class="sidebar" style="height: auto;">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">MAIN NAVIGATION</li>
      <li class="treeview">
        <a href="${host}/dashboard">
          <i class="fa fa-dashboard"></i>
          <span>Dashboard</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-files-o"></i>
          <span>Master Data</span>
        </a>
         <ul class="treeview-menu">
            <li>
              <a href="${host}/chekposts">
                <i class="fa fa-circle-o"></i>
               Check Posts
              </a>
            </li>

            <li>
              <a href="${host}/trips">
                <i class="fa fa-circle-o"></i>
                Distance & Duration
              </a>
            </li>

          <!-- <c:if test="${pms['WMasterCity'] eq true}">
            <li>
              <a href="${host}/admin/masters/city">
                <i class="fa fa-circle-o"></i>
                City
              </a>
            </li>
          </c:if>

          <c:if test="${pms['WMasterBranch'] eq true}">
            <li>
              <a href="${host}/admin/masters/branch">
                <i class="fa fa-circle-o"></i>
                Branch
              </a>
            </li>
          </c:if>
          <c:if test="${pms['WMasterArea'] eq true}">
            <li>
              <a href="${host}/admin/masters/area">
                <i class="fa fa-circle-o"></i>
                Area
              </a>
            </li>
          </c:if> -->
        </ul> 
      </li>


      <!--<c:if test="${pms['WManageRoles'] eq true}">
        <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>Access Management</span>
          </a>
           <ul class="treeview-menu">
            <li>
              <a href="${host}/admin/role_management/roles">
                <i class="fa fa-circle-o"></i>
                Manage Roles
              </a>
            </li>
            <li>
              <a href="${host}/admin/role_management/permissions">
                <i class="fa fa-circle-o"></i>
                Manage Permissions
              </a>
            </li>
            <li>
              <a href="${host}/admin/role_management/role_permissions">
                <i class="fa fa-circle-o"></i>
                Roles -> Permissions
              </a>
            </li>
          </ul> 
        </li>
      </c:if>-->

      <!-- <c:if test="${pms['WMasterProduct'] eq true}">
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i>
            <span>Manage Products</span>
          </a>
          <ul class="treeview-menu">
            <li>
              <a href="${host}/admin/mfin/product/search">
                <i class="fa fa-circle-o"></i>
                View &amp; Add Products
              </a>
            </li>

          </ul>
        </li>
      </c:if> -->

     <!--  <li class="treeview">
        <a href="${host}/managetrips">
          <i class="fa fa-folder"></i>
          <span> Manage Trips</span>
        </a>
      </li>
 -->

     <!--  <c:if test="${pms['WMasterAgent'] eq true}">
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Manage Agents</span>
          </a>
          <ul class="treeview-menu">
            <li>
              <a href="${host}/admin/agent/search">
                <i class="fa fa-circle-o"></i>
                Search &amp; Add Agents
              </a>
            </li>

            <c:if test="${pms['WTrackAgents'] eq true}">
              <li>
                <a href="${host}/admin/smartracker/devicesList">
                  <i class="fa fa-circle-o"></i>
                  Search &amp; Monitor Agents
                </a>
              </li>
            </c:if>
          </ul>
        </li>
      </c:if> -->

      <!-- <c:if test="${pms['WManageCustomers'] eq true}">
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i>
            <span>Manage Customers</span>
          </a>
          <ul class="treeview-menu">
            <li>
              <a href="${host}/admin/mfin/leads/search-batch">
                <i class="fa fa-circle-o"></i>
                New Leads
              </a>
            </li>
            <li>
              <a href="${host}/admin/mfin/enrollments/search-batch">
                <i class="fa fa-circle-o"></i>
                Customer Records
              </a>
            </li>
          </ul>
        </li>
      </c:if> -->


      


      <!-- <li>
        <a href="#" onclick="window.open('http://www.smargav.com')">
          <i class="fa fa-circle-o text-aqua"></i>
          <span>FieldX Help</span>
        </a>
      </li> -->
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>


