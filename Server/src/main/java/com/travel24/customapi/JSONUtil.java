package com.travel24.customapi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JSONUtil {
	
	public static final ObjectMapper MAPPER = new ObjectMapper();
	
	
	public static String jsonString(Object o) throws JsonProcessingException{
		try {
			return MAPPER.writeValueAsString(o);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			throw e;
		}
	}
}