package com.travel24.customapi;
import com.travel24.model.*;
import com.travel24.rest.TripDetailSearchReq;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

@Repository
public class CustomApi implements Custom{

	@PersistenceContext
	private EntityManager entityManager;

	public List<TripDetail> searchTrips(TripDetailSearchReq req){


		StringBuffer sb=new StringBuffer();

		sb.append("Select * from trip_details where created_time is not null");
		if(req.getFromDate()!=0){
			sb.append(" and entry_time>="+req.getFromDate());
		}
		if(req.getToDate()!=0){
			sb.append(" and entry_time<="+req.getToDate());
		}
	      if(req.getFromCp()!=0){
			sb.append(" and start_cp="+req.getFromCp());
		}
		if(req.getToCp()!=0){
			sb.append(" and end_cp="+req.getToCp());
		}

		System.out.println("SQL"+sb);
		Query listOfTrips=entityManager.createNativeQuery(sb.toString(), TripDetail.class);

		return  listOfTrips.getResultList();

	}



}
