package com.travel24.customapi;

import java.util.List;

import com.travel24.model.TripDetail;
import com.travel24.rest.TripDetailSearchReq;

public interface Custom {

	public List<TripDetail> searchTrips(TripDetailSearchReq req);

}
