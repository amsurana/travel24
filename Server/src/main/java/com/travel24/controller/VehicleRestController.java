package com.travel24.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.travel24.model.Vehicle;
import com.travel24.repo.CheckPostRepo;
import com.travel24.repo.VehicleRepo;
import com.travel24.rest.Request;
import com.travel24.rest.Response;
import com.travel24.rest.ResponseCodes;

@RestController
@RequestMapping("/vehicle")
public class VehicleRestController {

	
	@Autowired
	private VehicleRepo vehicleRepo;
	
	@Autowired
	private CheckPostRepo checkPostRepo;

	private static final Logger logs = Logger.getLogger(Vehicle.class);

	@RequestMapping(value = "add", method = RequestMethod.POST)
	public Response getAllUserDetails(@RequestBody Request<Vehicle> req) {
		Response resp = new Response();
		try {
			Vehicle vehicle = req.getRequest();
			if (vehicle != null) {
				vehicle.setCreatedTime(System.currentTimeMillis());
				vehicleRepo.save(vehicle);
				resp.setResp(null);
				resp.setCode(ResponseCodes.SUCCESS);
				resp.setMessage(ResponseCodes.SUCCESS_MSG);
			} else {
				resp.setResp("NULL_REQUEST_SENT");
				resp.setCode(ResponseCodes.FAILURE);
				resp.setMessage(ResponseCodes.FAILURE_MSG);
			}
		} catch (Exception e) {
			resp.setResp(e.getMessage());
			resp.setCode(ResponseCodes.FAILURE);
			resp.setMessage(ResponseCodes.FAILURE_MSG);
		}
		logs.info(resp.toJson());
		System.out.println(resp.toJson());
		return resp;
	}

	@RequestMapping(value = "by", method = RequestMethod.GET)
	public Response getVehicleById(@RequestParam("id") long id) {
		Response resp = new Response();
		try {
			resp.setResp(vehicleRepo.findOne(id));
			resp.setCode(ResponseCodes.SUCCESS);
			resp.setMessage(ResponseCodes.SUCCESS_MSG);
		} catch (Exception e) {
			resp.setResp(e.getMessage());
			resp.setCode(ResponseCodes.FAILURE);
			resp.setMessage(ResponseCodes.FAILURE_MSG);
		}
		logs.info(resp.toJson());
		System.out.println(resp.toJson());
		return resp;
	}

	@RequestMapping(value = "all", method = RequestMethod.GET)
	public Response getAllVehicles() {
		Response resp = new Response();
		try {
			List<Vehicle> getAllVehicles = vehicleRepo.findAll();
			if (getAllVehicles != null) {
				resp.setResp(getAllVehicles);
				resp.setCode(ResponseCodes.SUCCESS);
				resp.setMessage(ResponseCodes.SUCCESS_MSG);
			} else {
				resp.setResp(null);
				resp.setCode(500);
				resp.setMessage("Failure");

			}

		} catch (Exception e) {
			resp.setResp(e.getMessage());
			resp.setCode(ResponseCodes.FAILURE);
			resp.setMessage(ResponseCodes.FAILURE_MSG);
		}
		logs.info(resp.toJson());
		System.out.println(resp.toJson());
		return resp;
	}

}
