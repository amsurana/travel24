package com.travel24.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.travel24.model.MasterDetail;
import com.travel24.repo.CheckPostRepo;
import com.travel24.repo.MasterRepo;
import com.travel24.rest.Apis;
import com.travel24.rest.Request;
import com.travel24.rest.Response;
import com.travel24.rest.ResponseCodes;
@RestController
@RequestMapping(value="master")
public class MasterRestController {

	@Autowired
	private MasterRepo masterRepo;

	@Autowired
	private CheckPostRepo checkPostRepo;

	private static final Logger logs=Logger.getLogger(MasterRestController.class);

	@RequestMapping(value="add",method=RequestMethod.POST)
	public Response getAddMasterDetails(@RequestBody Request<MasterDetail> req){
        Response resp=new Response();
        try {
        	MasterDetail masterDetail=req.getRequest();
        	if(masterDetail!=null){
        	masterDetail.setCreatedTime(System.currentTimeMillis());
        	masterRepo.save(masterDetail);
			resp.setResp(null);
			resp.setCode(ResponseCodes.SUCCESS);
			resp.setMessage(ResponseCodes.SUCCESS_MSG);
        	}else{
        		resp.setResp("NULL_REQUEST_SENT");
    			resp.setCode(ResponseCodes.FAILURE);
    			resp.setMessage(ResponseCodes.FAILURE_MSG);
        	}
		} catch (Exception e) {
			resp.setResp(e.getMessage());
			resp.setCode(ResponseCodes.FAILURE);
			resp.setMessage(ResponseCodes.FAILURE_MSG);
		}
        logs.info(resp.toJson());
        System.out.println(resp.toJson());
		return resp;
	}


	@RequestMapping(value="all",method=RequestMethod.GET)
	public Response getAllPersonDetails(){
        Response resp=new Response();
        try {
			List<MasterDetail> getAllMasters=masterRepo.findAll();
            ArrayList<MasterDetail> masters=new ArrayList<MasterDetail>();
			for(MasterDetail master :getAllMasters){
            	master.setFromCheckPost(checkPostRepo.findOne(master.getFromCp()));
				master.setToCheckPost(checkPostRepo.findOne(master.getToCp()));
				masters.add(master);
            }
			resp.setResp(masters);
			resp.setCode(ResponseCodes.SUCCESS);
			resp.setMessage(ResponseCodes.SUCCESS_MSG);
		} catch (Exception e) {
			e.printStackTrace();
			resp.setResp(e.getMessage());
			resp.setCode(ResponseCodes.FAILURE);
			resp.setMessage(ResponseCodes.FAILURE_MSG);
		}
        logs.info(resp.toJson());
        System.out.println(resp.toJson());
		return resp;
	}

	@RequestMapping(value="delete",method=RequestMethod.GET)
	public Response deleteCheckPosts(@RequestParam("id") long id){
		Response resp=new Response();
		if(id!=0){
			try {
				masterRepo.delete(id);
				resp.setCode(ResponseCodes.SUCCESS);
				resp.setMessage(ResponseCodes.SUCCESS_MSG);
				resp.setResp(null);
			} catch (Exception e) {
		       e.printStackTrace();
		        resp.setCode(ResponseCodes.FAILURE);
				resp.setMessage(ResponseCodes.FAILURE_MSG);
				resp.setResp(e.getMessage());
			}

		}
		return resp;
	}
}
