package com.travel24.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.travel24.model.User;
import com.travel24.repo.CheckPostRepo;
import com.travel24.repo.UserRepo;
import com.travel24.rest.Apis;
import com.travel24.rest.Request;
import com.travel24.rest.Response;
import com.travel24.rest.ResponseCodes;
import com.travel24.rest.UserResp;


@RestController
@RequestMapping(value="user")
public class UserRestController {
    
	
	@Autowired
	private UserRepo userRepo;
	
	@Autowired
	private CheckPostRepo checkPostRepo;
	
	
	private static final Logger logs=Logger.getLogger(UserRestController.class);
	
	
	@RequestMapping(value="add",method=RequestMethod.POST)
	public Response getAllUserDetails(@RequestBody Request<User> req){
        Response resp=new Response();
        try {
        	User user=req.getRequest();
        	if(user!=null){
        	user.setCreatedTime(System.currentTimeMillis());	
			userRepo.save(user);
        	resp.setResp(null);
			resp.setCode(ResponseCodes.SUCCESS);
			resp.setMessage(ResponseCodes.SUCCESS_MSG);
        	}else{
        		resp.setResp("NULL_REQUEST_SENT");
    			resp.setCode(ResponseCodes.FAILURE);
    			resp.setMessage(ResponseCodes.FAILURE_MSG);
        	}
		} catch (Exception e) {
			resp.setResp(e.getMessage());
			resp.setCode(ResponseCodes.FAILURE);
			resp.setMessage(ResponseCodes.FAILURE_MSG);
		}
        logs.info(resp.toJson());
        System.out.println(resp.toJson());
		return resp;
	}
	
	
	
	
	@RequestMapping(value=Apis.GET_ALL_USER,method=RequestMethod.GET)
	public Response getAllPersonDetails(){
        Response resp=new Response();
        try {
			List<User> getAllUsers=userRepo.findAll();
            ArrayList<UserResp> users=new ArrayList<UserResp>();
			for(User user :getAllUsers){
            	UserResp userObj=new UserResp();
            	userObj.setUser(user);
                userObj.setCheckPost(checkPostRepo.findOne(user.getCheckPost()));
                users.add(userObj);
            }
			resp.setResp(users);
			resp.setCode(ResponseCodes.SUCCESS);
			resp.setMessage(ResponseCodes.SUCCESS_MSG);
		} catch (Exception e) {
			resp.setResp(e.getMessage());
			resp.setCode(ResponseCodes.FAILURE);
			resp.setMessage(ResponseCodes.FAILURE_MSG);
		}
        logs.info(resp.toJson());
        System.out.println(resp.toJson());
		return resp;
	}
	

	
}
