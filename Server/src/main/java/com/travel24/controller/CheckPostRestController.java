package com.travel24.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.travel24.model.CheckPost;
import com.travel24.model.TripDetail;
import com.travel24.repo.CheckPostRepo;
import com.travel24.rest.Request;
import com.travel24.rest.Response;
import com.travel24.rest.ResponseCodes;
@RestController
@RequestMapping("/checkpost")
public class CheckPostRestController {
	
	private static final Logger logs=Logger.getLogger(CheckPostRestController.class);
	
	@Autowired
	private CheckPostRepo checkPostRepo;
	

	@RequestMapping(value="add",method=RequestMethod.POST)
	public Response getAllPersonDetails(@RequestBody Request<CheckPost> req){
        Response resp=new Response();
        try {
        	CheckPost checkPost=req.getRequest();
        	if(checkPost!=null){
        	checkPost.setCreatedTime(System.currentTimeMillis());
            checkPostRepo.save(checkPost);
			resp.setResp(null);
			resp.setCode(ResponseCodes.SUCCESS);
			resp.setMessage(ResponseCodes.SUCCESS_MSG);
        	}else{
        		resp.setResp("NULL_REQUEST_SENT");
    			resp.setCode(ResponseCodes.FAILURE);
    			resp.setMessage(ResponseCodes.FAILURE_MSG);
        	}
		} catch (Exception e) {
			resp.setResp(e.getMessage());
			resp.setCode(ResponseCodes.FAILURE);
			resp.setMessage(ResponseCodes.FAILURE_MSG);
		}
        logs.info(resp.toJson());
        System.out.println(resp.toJson());
		return resp;
	}
	
	
	
	
	@RequestMapping(value="all",method=RequestMethod.GET)
	public Response getAllCheckPosts(){
		Response resp=new Response();
		List<CheckPost> listOfCPs=checkPostRepo.findAll();
		if(listOfCPs!=null){
			resp.setResp(listOfCPs);
			resp.setCode(ResponseCodes.SUCCESS);
			resp.setMessage(ResponseCodes.SUCCESS_MSG);
			
		}else{
			resp.setCode(ResponseCodes.FAILURE);
			resp.setMessage(ResponseCodes.FAILURE_MSG);
			resp.setResp(null);
		}
		return resp;
	}
	
	@RequestMapping(value="delete",method=RequestMethod.GET)
	public Response deleteCheckPosts(@RequestParam("id") long id){
		Response resp=new Response();
		if(id!=0){
			try {
				checkPostRepo.delete(id);
				resp.setCode(ResponseCodes.SUCCESS);
				resp.setMessage(ResponseCodes.SUCCESS_MSG);
				resp.setResp(null);
			} catch (Exception e) {
		       e.printStackTrace();
		        resp.setCode(ResponseCodes.FAILURE);
				resp.setMessage(ResponseCodes.FAILURE_MSG);
				resp.setResp(e.getMessage());
			}
		}
		return resp;
	}
	
	
	@RequestMapping(value="update",method=RequestMethod.POST)
	public Response updateCheckPosts(@RequestBody Request<CheckPost> req){
        Response resp=new Response();
        try {
        	checkPostRepo.save(req.getRequest());
        	resp.setMessage(ResponseCodes.SUCCESS_MSG);
			resp.setResp(null);
			resp.setCode(ResponseCodes.SUCCESS);
		} catch (Exception e) {
			resp.setResp(e.getMessage());
			resp.setCode(ResponseCodes.SUCCESS);
			resp.setMessage(ResponseCodes.FAILURE_MSG);
		}
        logs.info(resp.toJson());
        System.out.println(resp.toJson());
		return resp;
	}
	
}
