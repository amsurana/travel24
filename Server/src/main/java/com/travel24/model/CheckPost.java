package com.travel24.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigInteger;


/**
 * The persistent class for the check_posts database table.
 * 
 */
@Entity
@Table(name="check_posts")
@NamedQuery(name="CheckPost.findAll", query="SELECT c FROM CheckPost c")
public class CheckPost implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	@Column(name="created_time")
	private long createdTime;
	
	private String name;

	@Column(name="updated_time")
	private long updatedTime;

	//bi-directional one-to-one association to User
	/*@OneToOne(mappedBy="checkPost")
	private User user;
*/
	public CheckPost() {
	}

	

	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public long getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(long createdTime) {
		this.createdTime = createdTime;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getUpdatedTime() {
		return updatedTime;
	}

	public void setUpdatedTime(long updatedTime) {
		this.updatedTime = updatedTime;
	}


	

}