package com.travel24.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigInteger;


/**
 * The persistent class for the master_details database table.
 *
 */
@Entity
@Table(name="master_details")
@NamedQuery(name="MasterDetail.findAll", query="SELECT m FROM MasterDetail m")
public class MasterDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	@Column(name="created_time")
	private long createdTime;

	private long distance;

	@Column(name="from_cp")
	private long fromCp;

	@Column(name="to_cp")
	private long toCp;

	@Column(name="time_taken")
	private long timeTaken;

	@Column(name="updated_time")
	private long updatedTime;


	//Satish Added Code for Sending response
	@Transient
	private CheckPost fromCheckPost;
	@Transient
	private CheckPost toCheckPost;



	public MasterDetail() {
	}



	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public long getCreatedTime() {
		return createdTime;
	}



	public void setCreatedTime(long createdTime) {
		this.createdTime = createdTime;
	}



	public long getDistance() {
		return distance;
	}



	public void setDistance(long distance) {
		this.distance = distance;
	}



	public long getFromCp() {
		return fromCp;
	}



	public void setFromCp(long fromCp) {
		this.fromCp = fromCp;
	}



	public long getToCp() {
		return toCp;
	}



	public void setToCp(long toCp) {
		this.toCp = toCp;
	}



	public long getTimeTaken() {
		return timeTaken;
	}



	public void setTimeTaken(long timeTaken) {
		this.timeTaken = timeTaken;
	}



	public long getUpdatedTime() {
		return updatedTime;
	}



	public void setUpdatedTime(long updatedTime) {
		this.updatedTime = updatedTime;
	}



	public CheckPost getFromCheckPost() {
		return fromCheckPost;
	}



	public void setFromCheckPost(CheckPost fromCheckPost) {
		this.fromCheckPost = fromCheckPost;
	}



	public CheckPost getToCheckPost() {
		return toCheckPost;
	}



	public void setToCheckPost(CheckPost toCheckPost) {
		this.toCheckPost = toCheckPost;
	}



}