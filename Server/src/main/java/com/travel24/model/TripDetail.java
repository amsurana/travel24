package com.travel24.model;

import java.io.Serializable;

import javax.persistence.*;

import java.math.BigInteger;


/**
 * The persistent class for the trip_details database table.
 *
 */
@Entity
@Table(name="trip_details")
@NamedQueries({
	@NamedQuery(name="TripDetail.findAll", query="SELECT t FROM TripDetail t"),
	@NamedQuery(name="Trip.search",query="SELECT t from TripDetail t where t.createdTime>=?1 and t.createdTime<=?2 and t.startCp=?3 and t.endCp=?4")
})

public class TripDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	@Column(name="Delay")
	private byte delay;

	@Column(name="driver_name")
	private String driverName;

	@Column(name="end_cp")
	private long endCp;

	@Column(name="entry_time")
	private long entryTime;

	@Column(name="exit_time")
	private long exitTime;

	@Column(name="number_of_passenger")
	private long numberOfPassenger;

	@Column(name="Over")
	private byte over;

	@Column(name="Remark")
	private String remark;

	@Column(name="start_cp")
	private long startCp;


	//bi-directional one-to-one association to Vehicle
	//@OneToOne
	@Column(name="vehicles_id")
	private long vehicleId;

	@Column(name="created_time")
	private long createdTime;

	@Transient
	private String vehicleNo;

	@Transient
	private String vehicleType;

	@Transient
	private CheckPost fromCheckPost;

	@Transient
	private CheckPost endCheckPost;

	@Transient
	private Vehicle vehicleDetails;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public byte getDelay() {
		return delay;
	}

	public void setDelay(byte delay) {
		this.delay = delay;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public long getEndCp() {
		return endCp;
	}

	public void setEndCp(long endCp) {
		this.endCp = endCp;
	}

	public long getEntryTime() {
		return entryTime;
	}

	public void setEntryTime(long entryTime) {
		this.entryTime = entryTime;
	}

	public long getExitTime() {
		return exitTime;
	}

	public void setExitTime(long exitTime) {
		this.exitTime = exitTime;
	}

	public Vehicle getVehicleDetails() {
		return vehicleDetails;
	}

	public void setVehicleDetails(Vehicle vehicleDetails) {
		this.vehicleDetails = vehicleDetails;
	}

	public long getNumberOfPassenger() {
		return numberOfPassenger;
	}

	public void setNumberOfPassenger(long numberOfPassenger) {
		this.numberOfPassenger = numberOfPassenger;
	}

	public byte getOver() {
		return over;
	}

	public void setOver(byte over) {
		this.over = over;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public long getStartCp() {
		return startCp;
	}

	public void setStartCp(long startCp) {
		this.startCp = startCp;
	}

	public long getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(long vehicleId) {
		this.vehicleId = vehicleId;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public CheckPost getFromCheckPost() {
		return fromCheckPost;
	}

	public void setFromCheckPost(CheckPost fromCheckPost) {
		this.fromCheckPost = fromCheckPost;
	}

	public CheckPost getEndCheckPost() {
		return endCheckPost;
	}

	public void setEndCheckPost(CheckPost endCheckPost) {
		this.endCheckPost = endCheckPost;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public long getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(long createdTime) {
		this.createdTime = createdTime;
	}


}