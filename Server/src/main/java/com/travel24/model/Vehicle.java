package com.travel24.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigInteger;


/**
 * The persistent class for the vehicles database table.
 * 
 */
@Entity
@Table(name="vehicles")
@NamedQueries({
	@NamedQuery(name="Vehicle.findAll", query="SELECT v FROM Vehicle v"),
	@NamedQuery(name="Vehicle.GetByName",query="SELECT v FROM Vehicle v where v.number=?1")
})
public class Vehicle implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	@Column(name="created_time")
	private long createdTime;
     
	private String type;

	private String number;

	@Column(name="updated_time")
	private BigInteger updatedTime;
	
	

	//bi-directional one-to-one association to TripDetail
	/*@OneToOne(mappedBy="vehicle")
	private TripDetail tripDetail;
*/
	public Vehicle() {
	}

	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}


	public long getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(long createdTime) {
		this.createdTime = createdTime;
	}

	

	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getNumber() {
		return this.number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public BigInteger getUpdatedTime() {
		return this.updatedTime;
	}

	public void setUpdatedTime(BigInteger updatedTime) {
		this.updatedTime = updatedTime;
	}


}