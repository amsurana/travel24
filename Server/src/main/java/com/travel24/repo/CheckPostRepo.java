package com.travel24.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.travel24.model.CheckPost;

public interface CheckPostRepo extends JpaRepository<CheckPost, Long>{

	
}
