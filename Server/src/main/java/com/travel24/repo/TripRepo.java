package com.travel24.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.travel24.model.TripDetail;
import com.travel24.model.Vehicle;

public interface TripRepo extends JpaRepository<TripDetail, Long>{

	 List<TripDetail> findByEndCp(long endCp);
	 List<TripDetail> findByEndCpAndEntryTime(long endCp, long entryTime);
	 //List<TripDetail> findByStartCp(long startCp);

}
