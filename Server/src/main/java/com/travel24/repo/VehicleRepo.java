package com.travel24.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.travel24.model.Vehicle;

public interface VehicleRepo extends JpaRepository<Vehicle, Long> {

    Vehicle findByNumber(String number);
	
}
