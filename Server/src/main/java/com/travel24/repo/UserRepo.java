package com.travel24.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.travel24.model.User;

public interface UserRepo extends JpaRepository<User, Long>{
     
    
}
