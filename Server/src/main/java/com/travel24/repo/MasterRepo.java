package com.travel24.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.travel24.model.MasterDetail;

public interface MasterRepo extends JpaRepository<MasterDetail, Long> {


}
