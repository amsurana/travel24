package com.travel24.rest;

import com.travel24.model.CheckPost;
import com.travel24.model.User;

public class UserResp {

	private User user;
	
	private CheckPost checkPost;
	
	

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public CheckPost getCheckPost() {
		return checkPost;
	}

	public void setCheckPost(CheckPost checkPost) {
		this.checkPost = checkPost;
	}
	
}
