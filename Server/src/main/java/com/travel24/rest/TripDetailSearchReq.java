package com.travel24.rest;

public class TripDetailSearchReq {

	private long fromDate,toDate;
	private long fromCp,toCp;
	public long getFromDate() {
		return fromDate;
	}
	public void setFromDate(long fromDate) {
		this.fromDate = fromDate;
	}
	public long getToDate() {
		return toDate;
	}
	public void setToDate(long toDate) {
		this.toDate = toDate;
	}
	public long getFromCp() {
		return fromCp;
	}
	public void setFromCp(long fromCp) {
		this.fromCp = fromCp;
	}
	public long getToCp() {
		return toCp;
	}
	public void setToCp(long toCp) {
		this.toCp = toCp;
	}



}
