package com.abhiyana.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.math.BigInteger;

/**
 * The persistent class for the taluk_target_monthly database table.
 *
 */
@Entity
@Table(name = "taluk_target_monthly")
@NamedQuery(name = "TalukTargetMonthly.findAll", query = "SELECT t FROM TalukTargetMonthly t")

@SqlResultSetMapping(name = "DairySearchResponse", classes = {
		@ConstructorResult(targetClass = com.abhiyana.formobj.DairySearchResponse.class, columns = {
				@ColumnResult(name = "talukName", type = String.class),
				@ColumnResult(name = "month", type = Integer.class), @ColumnResult(name = "year", type = Integer.class),
				@ColumnResult(name = "taluk_year_target", type = Integer.class),
				@ColumnResult(name = "taluk_fin_target", type = Integer.class),
				@ColumnResult(name = "achieved", type = Integer.class),
				@ColumnResult(name = "bhautikeSubsidy", type = Integer.class),
				@ColumnResult(name = "bhautikeBankLoan", type = Integer.class),
				@ColumnResult(name = "arthikeSubsidy", type = Integer.class),
				@ColumnResult(name = "arthikeBankLoan", type = Integer.class),
				@ColumnResult(name = "women", type = Integer.class) }) })

public class TalukTargetMonthly implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "taluk_target_mnth_id")
	private int talukTargetMnthId;

	private BigInteger achieved;

	private BigInteger bhautikeBankLoan;

	// Subsidy
	private BigInteger bhautikeSubsidy;

	private BigInteger arthikeBankLoan;

	// Subsidy
	private BigInteger arthikeSubsidy;

	private int month;

	private Timestamp updatedTime;

	private int userId;

	private BigInteger women;

	private int year;

	private int talukId;

	public TalukTargetMonthly() {
	}

	public int getTalukTargetMnthId() {
		return this.talukTargetMnthId;
	}

	public void setTalukTargetMnthId(int talukTargetMnthId) {
		this.talukTargetMnthId = talukTargetMnthId;
	}

	public BigInteger getAchieved() {
		return this.achieved;
	}

	public void setAchieved(BigInteger achieved) {
		this.achieved = achieved;
	}

	public BigInteger getBhautikeBankLoan() {
		return bhautikeBankLoan;
	}

	public void setBhautikeBankLoan(BigInteger bhautikeBankLoan) {
		this.bhautikeBankLoan = bhautikeBankLoan;
	}

	public BigInteger getBhautikeSubsidy() {
		return bhautikeSubsidy;
	}

	public void setBhautikeSubsidy(BigInteger bhautikeSubsidy) {
		this.bhautikeSubsidy = bhautikeSubsidy;
	}

	public int getMonth() {
		return this.month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public Timestamp getUpdatedTime() {
		return this.updatedTime;
	}

	public void setUpdatedTime(Timestamp updatedTime) {
		this.updatedTime = updatedTime;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public BigInteger getWomen() {
		return this.women;
	}

	public void setWomen(BigInteger women) {
		this.women = women;
	}

	public int getYear() {
		return this.year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getTalukId() {
		return talukId;
	}

	public void setTalukId(int talukId) {
		this.talukId = talukId;
	}

	public BigInteger getArthikeBankLoan() {
		return arthikeBankLoan;
	}

	public void setArthikeBankLoan(BigInteger arthikeBankLoan) {
		this.arthikeBankLoan = arthikeBankLoan;
	}

	public BigInteger getArthikeSubsidy() {
		return arthikeSubsidy;
	}

	public void setArthikeSubsidy(BigInteger arthikeSubsidy) {
		this.arthikeSubsidy = arthikeSubsidy;
	}

}