package com.abhiyana.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;

/**
 * The persistent class for the taluk_target_year database table.
 *
 */
@Entity
@Table(name = "taluk_target_year")
@NamedQuery(name = "TalukTargetYear.findAll", query = "SELECT t FROM TalukTargetYear t")

public class TalukTargetYear implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "taluk_target_id")
	private int talukTargetId;

	@Column(name = "taluk_fin_target")
	private int talukFinTarget;

	@Column(name = "taluk_year_target")
	private int talukYearTarget;

	private Timestamp updatedTime;

	private int userId;

	private int year;

	private int talukId;

	public TalukTargetYear() {
	}

	public int getTalukTargetId() {
		return this.talukTargetId;
	}

	public void setTalukTargetId(int talukTargetId) {
		this.talukTargetId = talukTargetId;
	}

	public int getTalukFinTarget() {
		return this.talukFinTarget;
	}

	public void setTalukFinTarget(int talukFinTarget) {
		this.talukFinTarget = talukFinTarget;
	}

	public int getTalukYearTarget() {
		return this.talukYearTarget;
	}

	public void setTalukYearTarget(int talukYearTarget) {
		this.talukYearTarget = talukYearTarget;
	}

	public Timestamp getUpdatedTime() {
		return this.updatedTime;
	}

	public void setUpdatedTime(Timestamp updatedTime) {
		this.updatedTime = updatedTime;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getYear() {
		return this.year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getTalukId() {
		return talukId;
	}

	public void setTalukId(int talukId) {
		this.talukId = talukId;
	}

}