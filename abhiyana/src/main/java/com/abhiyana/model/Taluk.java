package com.abhiyana.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

/**
 * The persistent class for the taluk_table database table.
 *
 */
@Entity
@Table(name = "taluk_table")
@NamedQuery(name = "Taluk.findAll", query = "SELECT t FROM Taluk t")
public class Taluk implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int talukId;

	private String talukName;

	private Timestamp updatedTime;

	private int regionId;

	public Taluk() {
	}

	public int getTalukId() {
		return this.talukId;
	}

	public void setTalukId(int talukId) {
		this.talukId = talukId;
	}

	public String getTalukName() {
		return this.talukName;
	}

	public void setTalukName(String talukName) {
		this.talukName = talukName;
	}

	public Timestamp getUpdatedTime() {
		return this.updatedTime;
	}

	public void setUpdatedTime(Timestamp updatedTime) {
		this.updatedTime = updatedTime;
	}

	public int getRegionId() {
		return regionId;
	}

	public void setRegionId(int regionId) {
		this.regionId = regionId;
	}

}