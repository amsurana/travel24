package com.abhiyana.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the user_table database table.
 *
 */
@Entity
@Table(name="user_table")
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String username;

	private long createdTime;

	private String password;

	private int regionId;

	private String role;

	private int talukId;

	public User() {
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public long getCreatedTime() {
		return this.createdTime;
	}

	public void setCreatedTime(long createdTime) {
		this.createdTime = createdTime;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getRegionId() {
		return this.regionId;
	}

	public void setRegionId(int regionId) {
		this.regionId = regionId;
	}

	public String getRole() {
		return this.role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public int getTalukId() {
		return this.talukId;
	}

	public void setTalukId(int talukId) {
		this.talukId = talukId;
	}

}