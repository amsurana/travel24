package com.abhiyana.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.abhiyana.formobj.ChangePassword;
import com.abhiyana.formobj.Request;
import com.abhiyana.formobj.Response;
import com.abhiyana.formobj.ResponseCodes;
import com.abhiyana.formobj.UserResp;
import com.abhiyana.model.User;
import com.abhiyana.repo.RegionRepo;
import com.abhiyana.repo.UserRepo;

@RestController
@RequestMapping(value = "user")
public class UserRestController {

	@Autowired
	private UserRepo userRepo;

	@Autowired
	private RegionRepo regionRepo;

	private static final Logger logs = Logger.getLogger(UserRestController.class);

	@RequestMapping(value = "login", method = RequestMethod.POST)
	public Response userLogin(@RequestBody Request<User> req) {
		Response resp = new Response();
		try {
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			User usercheck = userRepo.findByUsername(req.getRequest().getUsername());
			if (usercheck != null && passwordEncoder.matches(req.getRequest().getPassword(), usercheck.getPassword())) {
				resp.setResp(usercheck);
				resp.setCode(ResponseCodes.SUCCESS);
				resp.setMessage(ResponseCodes.SUCCESS_MSG);
			} else {
				resp.setResp(null);
				resp.setCode(ResponseCodes.FAILURE);
				resp.setMessage(ResponseCodes.FAILURE_MSG);
			}
		} catch (Exception e) {
			resp.setResp(e.getMessage());
			resp.setCode(ResponseCodes.FAILURE);
			resp.setMessage(ResponseCodes.FAILURE_MSG);
		}
		logs.info(resp.toJson());
		System.out.println(resp.toJson());
		return resp;
	}

	@RequestMapping(value = "change_password", method = RequestMethod.POST)
	public Response changePassword(@RequestBody Request<ChangePassword> req) {
		Response resp = new Response();
		try {
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			User usercheck = userRepo.findByUsername(req.getRequest().getName());
			if (usercheck != null
					&& passwordEncoder.matches(req.getRequest().getCurrentPassword(), usercheck.getPassword())) {
				usercheck.setPassword(passwordEncoder.encode(req.getRequest().getNewPassword()));
				userRepo.save(usercheck);
				resp.setResp(usercheck);
				resp.setCode(ResponseCodes.SUCCESS);
				resp.setMessage(ResponseCodes.SUCCESS_MSG);
			} else {
				resp.setResp(null);
				resp.setCode(ResponseCodes.FAILURE);
				resp.setMessage(ResponseCodes.FAILURE_MSG);
			}
		} catch (Exception e) {
			resp.setResp(e.getMessage());
			resp.setCode(ResponseCodes.FAILURE);
			resp.setMessage(ResponseCodes.FAILURE_MSG);
		}
		logs.info(resp.toJson());
		System.out.println(resp.toJson());
		return resp;
	}

	@RequestMapping(value = "add", method = RequestMethod.POST)
	public Response getAllUserDetails(@RequestBody Request<User> req) {
		Response resp = new Response();
		try {
			User user = req.getRequest();
			if (user != null && !userRepo.exists(user.getUsername())) {
	        	BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
	            user.setPassword(passwordEncoder.encode(user.getPassword()));
				user.setCreatedTime(System.currentTimeMillis());
				userRepo.save(user);
				resp.setResp(null);
				resp.setCode(ResponseCodes.SUCCESS);
				resp.setMessage(ResponseCodes.SUCCESS_MSG);
			} else {
				resp.setResp("Request exists or invalid request");
				resp.setCode(ResponseCodes.FAILURE);
				resp.setMessage(ResponseCodes.FAILURE_MSG);
			}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setResp(e.getMessage());
			resp.setCode(ResponseCodes.FAILURE);
			resp.setMessage(ResponseCodes.FAILURE_MSG);
		}
		logs.info(resp.toJson());
		System.out.println(resp.toJson());
		return resp;
	}

	@RequestMapping(value = "all", method = RequestMethod.GET)
	public Response getAllPersonDetails() {
		Response resp = new Response();
		try {
			List<User> getAllUsers = userRepo.findAll();
			ArrayList<UserResp> users = new ArrayList<UserResp>();
			for (User user : getAllUsers) {
				UserResp userObj = new UserResp();
				userObj.setUser(user);
				users.add(userObj);
			}
			resp.setResp(users);
			resp.setCode(ResponseCodes.SUCCESS);
			resp.setMessage(ResponseCodes.SUCCESS_MSG);
		} catch (Exception e) {
			resp.setResp(e.getMessage());
			resp.setCode(ResponseCodes.FAILURE);
			resp.setMessage(ResponseCodes.FAILURE_MSG);
		}
		logs.info(resp.toJson());
		System.out.println(resp.toJson());
		return resp;
	}

}
