package com.abhiyana.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abhiyana.formobj.Request;
import com.abhiyana.formobj.Response;
import com.abhiyana.formobj.ResponseCodes;
import com.abhiyana.model.TalukTargetMonthly;
import com.abhiyana.repo.RegionRepo;
import com.abhiyana.repo.TalukRepo;
import com.abhiyana.repo.TalukTMRepo;

@RestController
@RequestMapping(value = "taluk")
public class MasterDataRestController {

	private static final Logger logs = Logger.getLogger(UserRestController.class);

	@Autowired
	private TalukTMRepo monthlyReportRepo;

	@Autowired
	private RegionRepo regionRepo;

	@Autowired
	private TalukRepo talukRepo;

	@RequestMapping(value = "savemonthlyreport", method = RequestMethod.POST)
	public Response fillMonthly(@RequestBody Request<TalukTargetMonthly> req) {
		Response resp = new Response();
		try {

			TalukTargetMonthly request = req.getRequest();
			TalukTargetMonthly found = monthlyReportRepo.findByMonthAndYearAndTalukId(request.getMonth(),
					request.getYear(), request.getTalukId());
			if (found == null) {
				monthlyReportRepo.save(request);
				resp.setResp(request);
				resp.setCode(ResponseCodes.SUCCESS);
				resp.setMessage(ResponseCodes.SUCCESS_MSG);
			} else {
				resp.setResp(null);
				resp.setCode(ResponseCodes.FAILURE);
				resp.setMessage("Report already filled");
			}
		} catch (Exception e) {
			resp.setResp(e.getMessage());
			resp.setCode(ResponseCodes.FAILURE);
			resp.setMessage(ResponseCodes.FAILURE_MSG);
		}
		logs.info(resp.toJson());
		System.out.println(resp.toJson());
		return resp;
	}

	@RequestMapping(value = "allregions", method = RequestMethod.GET)
	public Response getAllRegions() {
		Response resp = new Response();

		try {
			resp.setResp(regionRepo.findAll());
			resp.setCode(ResponseCodes.SUCCESS);
		} catch (Exception e) {
			resp.setResp(e.getMessage());
			resp.setCode(ResponseCodes.FAILURE);
			resp.setMessage(ResponseCodes.FAILURE_MSG);
		}
		return resp;

	}

	@RequestMapping(value = "talukbyregion", method = RequestMethod.GET)
	public Response getAllTaluks(@RequestParam("regionId") int regionId) {
		Response resp = new Response();

		try {
			resp.setResp(talukRepo.findByRegionId(regionId));
			resp.setCode(ResponseCodes.SUCCESS);
		} catch (Exception e) {
			resp.setResp(e.getMessage());
			resp.setCode(ResponseCodes.FAILURE);
			resp.setMessage(ResponseCodes.FAILURE_MSG);
		}
		return resp;

	}
}
