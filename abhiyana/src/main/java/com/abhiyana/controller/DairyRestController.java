package com.abhiyana.controller;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.abhiyana.customapi.SearchUtil;
import com.abhiyana.formobj.Request;
import com.abhiyana.formobj.Response;
import com.abhiyana.formobj.ResponseCodes;
import com.abhiyana.formobj.SearchReq;
import com.abhiyana.model.TalukTargetMonthly;
import com.abhiyana.model.TalukTargetYear;
import com.abhiyana.repo.TalukTMRepo;
import com.abhiyana.repo.TalukTYRepo;

@RestController
@RequestMapping(value = "dairy")
public class DairyRestController {

	private static final Logger logs = Logger.getLogger(UserRestController.class);

	@Autowired
	private TalukTMRepo monthlyReportRepo;

	@Autowired
	private TalukTYRepo dairyYearlyRepo;

	@PersistenceContext
	private EntityManager entityManager;

	@RequestMapping(value = "search", method = RequestMethod.POST)
	public Response search(@RequestBody Request<SearchReq> req) {
		Response resp = new Response();
		try {

			SearchReq request = req.getRequest();
			Object result = processSearchRequest(request);
			resp.setResp(result);
			resp.setCode(ResponseCodes.SUCCESS);
		} catch (Exception e) {
			resp.setResp(e.getMessage());
			resp.setCode(ResponseCodes.FAILURE);
			resp.setMessage(ResponseCodes.FAILURE_MSG);
		}
		logs.info(resp.toJson());
		System.out.println(resp.toJson());
		return resp;
	}

	private Object processSearchRequest(SearchReq request) {
		if (request.regionId == -1) {
			return SearchUtil.searchForAllRegions(entityManager, request.year, request.month);
		} else if (request.regionId != 0) {
			return SearchUtil.searchByRegionId(entityManager, request.year, request.month, request.regionId);
		}
		return SearchUtil.searchByTalukId(entityManager, request.year, request.month, request.talukId);
	}

	@RequestMapping(value = "addYearlyTarget", method = RequestMethod.POST)
	public Response addYearlyTarget(@RequestBody Request<TalukTargetYear> req) {
		Response resp = new Response();
		try {

			TalukTargetYear request = req.getRequest();
			TalukTargetYear found = dairyYearlyRepo.findByYearAndTalukId(request.getYear(), request.getTalukId());
			if (found == null) {
				dairyYearlyRepo.save(request);
				resp.setResp(request);
				resp.setCode(ResponseCodes.SUCCESS);
				resp.setMessage(ResponseCodes.SUCCESS_MSG);
			} else {
				resp.setResp(null);
				resp.setCode(ResponseCodes.FAILURE);
				resp.setMessage("Targets already filled for this Taluk");
			}
		} catch (Exception e) {
			resp.setResp(e.getMessage());
			resp.setCode(ResponseCodes.FAILURE);
			resp.setMessage(ResponseCodes.FAILURE_MSG);
		}
		logs.info(resp.toJson());
		System.out.println(resp.toJson());
		return resp;
	}

	@RequestMapping(value = "addMonthlyTarget", method = RequestMethod.POST)
	public Response addMonthlyTarget(@RequestBody Request<TalukTargetMonthly> req) {
		Response resp = new Response();
		try {

			TalukTargetMonthly request = req.getRequest();
			TalukTargetMonthly found = monthlyReportRepo.findByMonthAndYearAndTalukId(request.getMonth(),
					request.getYear(), request.getTalukId());
			if (found == null) {
				monthlyReportRepo.save(request);
				resp.setResp(request);
				resp.setCode(ResponseCodes.SUCCESS);
				resp.setMessage(ResponseCodes.SUCCESS_MSG);
			} else {
				resp.setResp(null);
				resp.setCode(ResponseCodes.FAILURE);
				resp.setMessage("Targets already filled for this Taluk");
			}
		} catch (Exception e) {
			resp.setResp(e.getMessage());
			resp.setCode(ResponseCodes.FAILURE);
			resp.setMessage(ResponseCodes.FAILURE_MSG);
		}
		logs.info(resp.toJson());
		System.out.println(resp.toJson());
		return resp;
	}

	@RequestMapping(value = "getYearlyTarget", method = RequestMethod.GET)
	public Response getYearlyTarget(@RequestParam("year") int year, @RequestParam("talukId") int talukId) {
		Response resp = new Response();
		try {
			TalukTargetYear yearlyTarget = dairyYearlyRepo.findByYearAndTalukId(year, talukId);
			String formatted = String.format("<br>(3) Subsidy Target: %d" + "<br>(4) Financial Target: %d</p>",
					yearlyTarget.getTalukYearTarget(), yearlyTarget.getTalukFinTarget());
			resp.setMessage(formatted);
			resp.setCode(ResponseCodes.SUCCESS);
		} catch (Exception e) {
			resp.setResp(e.getMessage());
			resp.setCode(ResponseCodes.FAILURE);
			resp.setMessage(ResponseCodes.FAILURE_MSG);
		}
		return resp;

	}

}
