package com.abhiyana.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ViewController {

	@RequestMapping(value = "login", method = RequestMethod.GET)
	public ModelAndView login() {
		return new ModelAndView("login");
	}

	//District
	@RequestMapping(value = "dairyDistrictWiseReport", method = RequestMethod.GET)
	public ModelAndView dairyDistrictWiseReport() {
		return new ModelAndView("dairy/dairyDistrictWiseReport");
	}

	@RequestMapping(value = "dairyTalukWiseReport", method = RequestMethod.GET)
	public ModelAndView dairyTalukWiseReport() {
		return new ModelAndView("dairy/dairyTalukWiseReport");
	}

	@RequestMapping(value = "dairyMonthlyReport", method = RequestMethod.GET)
	public ModelAndView getTalukMR() {
		return new ModelAndView("dairy/fillMonthlyReport");
	}

	@RequestMapping(value = "dairyYearlyTargets", method = RequestMethod.GET)
	public ModelAndView getTalukYR() {
		return new ModelAndView("dairy/fillYearlyTargets");
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView getIndex() {
		return new ModelAndView("dashboard");
	}

	@RequestMapping(value = "dashboard", method = RequestMethod.GET)
	public ModelAndView getDashboard() {
		return new ModelAndView("dashboard");
	}

	@RequestMapping(value = "changepassword", method = RequestMethod.GET)
	public ModelAndView getChangePassword() {
		return new ModelAndView("changePassword");
	}

	@RequestMapping(value = "newregistration", method = RequestMethod.GET)
	public ModelAndView getNewRegistration() {
		return new ModelAndView("newRegistration");
	}

}