package com.abhiyana.customapi;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

public class SearchUtil {

	public static List searchByRegionId(EntityManager manager, int year, int month, int regionId) {
		String query = "SELECT m.*, t.talukName, y.taluk_fin_target, y.taluk_year_target FROM taluk_target_monthly m "
				//
				+ "INNER JOIN taluk_target_year y on m.talukId=y.talukId "
				//
				+ "INNER JOIN taluk_table t on m.talukId=t.talukId "
				//
				+ "WHERE y.year=%d "
				//
				+ "AND m.year=%d "
				//
				+ "AND m.month=%d "
				//
				+ "AND y.talukId IN (select talukId from taluk_table where regionId=%d)";
		String nativeQ = String.format(query, year, year, month, regionId);
		Query q = manager.createNativeQuery(nativeQ, "DairySearchResponse");
		List list = q.getResultList();
		return list;
	}

	public static List searchByTalukId(EntityManager manager, int year, int month, int talukId) {
		String query = "SELECT m.*, t.talukName, y.taluk_fin_target, y.taluk_year_target FROM taluk_target_monthly m "
				//
				+ "INNER JOIN taluk_target_year y on m.talukId=y.talukId "
				//
				+ "INNER JOIN taluk_table t on m.talukId=t.talukId "
				//
				+ "WHERE y.year=%d "
				//
				+ "AND m.year=%d "
				//
				+ "AND m.month=%d "
				//
				+ "AND y.talukId=%d";
		String nativeQ = String.format(query, year, year, month, talukId);
		Query q = manager.createNativeQuery(nativeQ, "DairySearchResponse");
		List list = q.getResultList();
		return list;
	}

	public static List searchForAllRegions(EntityManager manager, int year, int month) {
		String query = "SELECT m.*, t.talukName, y.taluk_fin_target, y.taluk_year_target FROM taluk_target_monthly m "
				//
				+ "INNER JOIN taluk_target_year y on m.talukId=y.talukId "
				//
				+ "INNER JOIN taluk_table t on m.talukId=t.talukId "
				//
				+ "WHERE y.year=%d "
				//
				+ "AND m.year=%d "
				//
				+ "AND m.month=%d ";
		String nativeQ = String.format(query, year, year, month);
		Query q = manager.createNativeQuery(nativeQ, "DairySearchResponse");
		List list = q.getResultList();
		return list;
	}
}
