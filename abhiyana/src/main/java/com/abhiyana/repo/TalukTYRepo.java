package com.abhiyana.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.abhiyana.model.TalukTargetYear;

public interface TalukTYRepo extends JpaRepository<TalukTargetYear, Long> {

	TalukTargetYear findByYearAndTalukId(int year, int talukId);


}
