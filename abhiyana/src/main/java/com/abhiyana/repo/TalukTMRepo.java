package com.abhiyana.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.abhiyana.model.TalukTargetMonthly;

public interface TalukTMRepo extends JpaRepository<TalukTargetMonthly, Long> {

	public TalukTargetMonthly findByMonthAndYearAndTalukId(int month, int year, int talukId);
}
