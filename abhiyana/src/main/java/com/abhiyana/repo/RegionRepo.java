package com.abhiyana.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.abhiyana.model.Region;

public interface RegionRepo extends JpaRepository<Region, Integer>{


}
