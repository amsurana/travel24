package com.abhiyana.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.abhiyana.model.Taluk;

public interface TalukRepo extends JpaRepository<Taluk, Integer> {

	List<Taluk> findByRegionId(int regionId);


}
