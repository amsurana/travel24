package com.abhiyana.formobj;

import com.abhiyana.model.User;

public class UserResp {

	private User user;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
