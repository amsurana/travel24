package com.abhiyana.formobj;

import com.abhiyana.customapi.JSONUtil;
import com.fasterxml.jackson.core.JsonProcessingException;

public class Response {
     
	private Object resp;
	private int code;
	private String message;
	public Object getResp() {
		return resp;
	}
	public void setResp(Object resp) {
		this.resp = resp;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public String toJson(){
		try {
			return JSONUtil.jsonString(this);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "ERROR - JSON Parsing";
	}
	
} 
