package com.abhiyana.formobj;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.SqlResultSetMapping;

@SqlResultSetMapping(name = "DairySearchResponse", classes = {
		@ConstructorResult(targetClass = com.abhiyana.formobj.DairySearchResponse.class, columns = {
				@ColumnResult(name = "talukName", type = String.class),
				@ColumnResult(name = "month", type = Integer.class),

				@ColumnResult(name = "year", type = Integer.class),
				@ColumnResult(name = "taluk_year_target", type = Integer.class),
				@ColumnResult(name = "taluk_fin_target", type = Integer.class),
				@ColumnResult(name = "achieved", type = Integer.class),
				@ColumnResult(name = "bhautikeSubsidy", type = Integer.class),
				@ColumnResult(name = "bhautikeBankLoan", type = Integer.class),
				@ColumnResult(name = "arthikeSubsidy", type = Integer.class),
				@ColumnResult(name = "arthikeBankLoan", type = Integer.class),
				@ColumnResult(name = "women", type = Integer.class) }) })
public class DairySearchResponse {

	public DairySearchResponse() {

	}

	public DairySearchResponse(String talukName, Integer month, Integer year, Integer taluk_year_target,
			Integer taluk_fin_target, Integer achieved, Integer bhautikeSubsidy, Integer bhautikeBankLoan,
			Integer arthikeSubsidy, Integer arthikeBankLoan, Integer women) {
		this.talukName = talukName;
		this.year = year;
		this.month = month;
		this.taluk_year_target = taluk_year_target;
		this.taluk_fin_target = taluk_fin_target;
		this.achieved = achieved;
		this.bhautikeSubsidy = bhautikeSubsidy;
		this.bhautikeBankLoan = bhautikeBankLoan;
		this.arthikeBankLoan = arthikeBankLoan;
		this.arthikeSubsidy = arthikeSubsidy;
		this.women = women;
	}

	public String regionName;
	public String talukName;
	public int year;
	public int month;
	public int taluk_year_target;
	public int taluk_fin_target;
	public int achieved;
	public int bhautikeSubsidy;
	public int bhautikeBankLoan;
	public int arthikeSubsidy;
	public int arthikeBankLoan;
	public int women;

}
