$(function() {
    showLoader();
    var login_status = localStorage.getItem("login_status");
    if (login_status == 1) {
        $.ajax({
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            url: "taluk/allregions",
        }).done(function(response) {
            hideLoader();

            loadRegionDatatable(response.resp);
        });


    } else {
        location.href = "/";
    }
    createRegion();
    updateRegion();

});

function loadRegionDatatable(dataSet) {
    if (typeof dataTable_region == 'undefined') {
        dataTable_region = $("#regionTable")
            .dataTable({
                "bDestroy": true,
                "bRetrieve": true,
                "bProcessing": true,
                "bDeferRender": true,
                "aaData": dataSet,
                "aoColumns": [{
                    "sTitle": "Id",
                    "mData": "id"

                }, {
                    "sTitle": "Name",
                    "mData": "name"
                }, {
                    "sTitle": "Action",
                    "mData": "data",
                    "mRender": function(datam, type, row) {
                        return "<button class='btn btn-default' data-toggle='modal'  id='editInfo'><i class='fa fa-pencil' aria-hidden='true '></i></button> &nbsp <button class='btn btn-default' id='deleteInfo'><i class='fa fa-trash' aria-hidden='true '></i></button>"
                    }
                }]
            })

    } else {
        dataTable_region.dataTable().fnClearTable();
        if (dataSet.length != 0) {

            dataTable_region.fnAddData(dataSet)

        }
    }
}

function createRegion() {
    showLoader();
    $("#create_cp_form").validate({
        rules: {
            cp_name: {
                required: true
            }

        },
        submitHandler: function(form) {
            //showLoader();
            // Defined in utility.js

            var cp_name = $("#cp_name").val();
            var data = {
                "request": {
                    "name": cp_name
                }
            }
            $.ajax({
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                url: window.location.origin + "/abhiyana/taluk/addregion",
                data: JSON.stringify(data)
            }).done(function(response) {
                hideLoader();
                $("#create_cp_form")[0].reset();
                location.reload();
            })
            return false;
        }
    });
}

$("#regionTable").on('click', '#editInfo', function(event) {
    $("#editPopup").modal("show");
    var row = $(this).closest("tr").get(0);
    var rowIndex = dataTable_region.fnGetPosition(row);
    var rowData = dataTable_region.fnGetData(rowIndex);
    var id = rowData.id;
    $("#regionId").val(id);
    $("#changeName") .val(rowData.name);

});

$("#regionTable").on('click', '#deleteInfo', function(event) {
    if( confirm("Do you want to delete region?? ")){
       showLoader();
        var row = $(this).closest("tr").get(0);
        var rowIndex = dataTable_region.fnGetPosition(row);
        var rowData = dataTable_region.fnGetData(rowIndex);
        var id = rowData.id;

        $.ajax({
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            url: "taluk/deleteregion?id=" + id,
        }).done(function(response) {
            hideLoader();
            alert("successfully deleted!");
            location.reload();
        })
    }else{
        return false
    }



});

function updateRegion() {
    showLoader();
    $("#editPopupForm").validate({
        rules: {

        },
        submitHandler: function(form) {
            var cp_name = $("#changeName").val();
            var id = $("#regionId").val();
            var data = {
                "request": {
                    "id": id,
                    "name": cp_name
                }
            }

            $.ajax({
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: JSON.stringify(data),
                url: "taluk/regionUpdate"
            }).done(function(response) {
                hideLoader();
                alert("successfully updated!");
                location.reload();
            })

            return false;
        }
    });
}
