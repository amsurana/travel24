$(function(){
	regUser();
})

function regUser() {

    $("#create_user").validate({

          rules: {},
        submitHandler: function(form) {
            showLoader();
            var userName=$("#username").val();
            var password=$("#password").val();
            var data = {
                "request": {
                        username:userName,
                        password:password,
                }
            }
            $.ajax({
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                url: "user/add",
                data: JSON.stringify(data)
            }).done(function(response) {
                hideLoader();
                if(response.code==200){
                     alert("Successfully Added !");
                    location.reload();
                }else{
                    alert("Oops Something Went Wrong!")
                }


            })
            return false;
        }
    });

}