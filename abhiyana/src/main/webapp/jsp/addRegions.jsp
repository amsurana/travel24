<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Add region</title>
</head>
<body>
     <%@ include file="header.jsp" %>
     <%@ include file="side-menu.jsp" %>
    <div class="content-wrapper" >
        <section class="content">
          <div class="row" style="margin:0px !important;">
                  <div class="box  panel box box-warning accessPermission" style="box-shadow: 0px 2px 10px #868282;">
                      <div class="box-header" data-widget="collapse-header">
                          <h3 class="box-title">Add Region</h3>
                          <div class="box-tools pull-right">
                              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                          </div>
                      </div><!-- /.box-header -->
                      <div class="box-body">
                         <form id="create_cp_form" name="create_cp_form">
                          <div class="form-group">
                              <h5 class="box-title">*Region Name:</h5>
                              <input type="text" class="form-control" id="cp_name" required data-msg="Please Enter Region Name">
                            </div>
                            <div class="form-group float-right" style="margin-right: 14px;">
                                 <input type="submit" class="btn btn-warning" id="deleteSubmit" value="Add"  />
                            </div>
                        </form>
                      </div><!-- /.box-body -->
              </div><!-- /.box -->
          </div>
          <div class="row">

              <div class="col-md-12">
                <div class="height10"></div>
                <div class=" tableSection sectionBgWhite">
                   <div class="height20"></div>
                   <table id="regionTable" class="stripe table table-bordered table-striped" style="width:100%">
                   </table>
                </div>
             </div>

          </div>



          <div class="modal fade" id="editPopup">
              <div class="modal-dialog">
                <div class="modal-content">
                <form id="editPopupForm" name="editPopupForm" action="">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">*Edit Region Name</h4>
                  </div>
                  <div class="modal-body">

                      <label>Region Name</label>
                      <input type="text" id="changeName" name="changeName" class="form-control form-group required" required  data-msg="Please Enter Region Name"/>
                      <input type="hidden" id="regionId">

                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-warning" id="changeSubmit" value="Update" />
                  </div>
                  </form>
                </div>
              </div>
          </div>
        </section>
        </div>
    </div>
</body>
</html>
<script type="text/javascript"
  src="<c:url value="/resources/js/add_regions.js" />"></script>