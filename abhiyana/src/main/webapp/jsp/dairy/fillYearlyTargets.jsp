<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Fill Yearly Targets</title>
</head>
<body>
	<%@ include file="../header.jsp"%>
	<%@ include file="../side-menu.jsp"%>
	<div class="content-wrapper">
		<section class="content">
		<div class="row" style="margin: 0px !important;">
			<div class="box  panel box box-warning accessPermission"
				style="box-shadow: 0px 2px 10px #868282;">
				<div class="box-header" data-widget="collapse-header">
					<h3 class="box-title">Fill Taluk-wise Yearly Targets</h3>
					<div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse">
							<i class="fa fa-minus"></i>
						</button>
					</div>
				</div>
				<!-- /.box-header -->
				<div class="box-body">

					<form class="form-horizontal row-border" id="submitForm">
						<div class="row">
							<div class="col-md-6 col-sm-3 col-xs-6">
								<div class="form-group">
									<label class="col-md-4 control-label" id="unameLabel">District
										Name:</label>
									<div class="col-md-8">
										<select class="form-control" id="regions" name="region">
											<option value="">--Select Region--</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" id="unameLabel">Taluk
										Name:</label>
									<div class="col-md-8">
										<select class="form-control" id="taluks" name="talukId">
											<option value="">--Select Taluk--</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" id="uname">Year :</label>
									<div class="col-md-8">
										<select class="form-control" id="years" name="year">
											<option value="2015">2015-16</option>
											<option value="2016">2016-17</option>
											<option value="2017">2017-18</option>
											<option value="2018">2018-19</option>
											<option value="2019">2019-20</option>
											<option value="2020">2020-21</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" id="uname">Subsidy
										Target:</label>
									<div class="col-md-8">
										<input name="talukYearTarget" class="form-control"
											placeholder="Enter yearly target" type="number">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" id="uname">Financial
										Target :</label>
									<div class="col-md-8">
										<input name="talukFinTarget" class="form-control"
											placeholder="Enter financial target" type="number">
									</div>
								</div>
							</div>

						</div>
						<div class="modal-footer">
							<div class="form-group last">
								<div class="col-sm-offset-3 col-sm-9">
									<button type="submit" class="btn btn-warning">Save</button>
								</div>
							</div>
						</div>

						<!-- <input name="userId" type="hidden" value="1"> -->
					</form>


				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>

		</section>
	</div>
	</div>
</body>
</html>
<script type="text/javascript">
	$(function() {
		loadRegions();
		$("#regions").change(function() {
			loadTaluksFor($("#regions").val());
		});

		$("#submitForm").validate({
			rules : {},
			submitHandler : submitForm
		});

	});

	function submitForm(form) {
		//showLoader();

		var data = {
			request : serializeObject($("#submitForm"))
		}

		console.log(data);
		$.ajax({
			type : "POST",
			contentType : 'application/json; charset=utf-8',
			dataType : 'json',
			url : "dairy/addYearlyTarget",
			data : JSON.stringify(data)
		}).done(function(response) {
			hideLoader();
			if (response.code == 200) {
				alert("Successfully Added !");
				location.reload();
			} else if (response.code == 500 && response.message) {
				alert(response.message);
			} else {
				alert("Oops Something Went Wrong!")
			}

		})
		return false;
	}
</script>