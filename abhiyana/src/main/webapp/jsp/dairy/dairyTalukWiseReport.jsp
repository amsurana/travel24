<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Abhiyana</title>
</head>
<body>
	<%@ include file="../header.jsp"%>
	<%@ include file="../side-menu.jsp"%>
	<div class="content-wrapper">
		<section class="content">
		<div class="row" style="margin: 0px !important;">
			<div class="box  panel box box-warning"
				style="box-shadow: 0px 2px 10px #868282;">
				<div class="box-header" data-widget="collapse-header">
					<h3 class="box-title">Search</h3>
					<div class="box-tools pull-right">
						<button class="btn btn-box-tool" data-widget="collapse">
							<i class="fa fa-minus"></i>
						</button>
					</div>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<form id="submitForm" name="submitForm">
						<div class="form-group col-md-3">
							<label for="cp_list_from">Month:</label>
							<div class='input-group Date optional'>
								<select class="form-control" id="months" name="month">
									<option value="1">January</option>
									<option value="2">February</option>
									<option value="3">March</option>
									<option value="4">April</option>
									<option value="5">May</option>
									<option value="6">June</option>
									<option value="7">July</option>
									<option value="8">August</option>
									<option value="9">September</option>
									<option value="10">October</option>
									<option value="11">November</option>
									<option value="12">December</option>
								</select>
							</div>
						</div>

						<div class="form-group col-md-3">
							<label for="cp_list-to">Year:</label>
							<div class='input-group Date optional'>
								<select class="form-control" id="year" name="year">
									<option value="2015">2015-16</option>
									<option value="2016">2016-17</option>
									<option value="2017">2017-18</option>
									<option value="2018">2018-19</option>
									<option value="2019">2019-20</option>
									<option value="2020">2020-21</option>
								</select>
							</div>
						</div>

						<div class="form-group col-md-3">
							<label for="trip_distance">Taluk:</label> <select
								class="form-control" id="taluks" name="talukId">
								<option value="-1">--All Taluks--</option>
							</select>
						</div>

						<div class="form-group float-right"
							style="margin-right: 14px; margin-top: 10px">
							<input type="submit" class="btn btn-warning" value="search" />
						</div>

					</form>
				</div>
			</div>
			<!-- /.box -->
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="height10"></div>
				<div class=" tableSection sectionBgWhite">
					<div class="height20"></div>
					<table id="reportsTable"
						class="stripe table table-bordered table-striped" width="100%">
					</table>
				</div>
			</div>
		</div>
		</section>
	</div>
</body>
</html>
<script type="text/javascript">
	var reportsTable;
	$(function() {
		var login_status = localStorage.getItem("login_status");
		if (login_status == 1) {
			var user = JSON.parse(localStorage.getItem("userProfile"));
			loadTaluksFor(user.regionId, taluksCallback);
		} else {
			window.location.replace(window.location.origin + "/abhiyana/");
		}

		$("#submitForm").validate({
			rules : {},
			submitHandler : performSearch
		});

	})

	function taluksCallback() {
		$('#taluks').prepend($('<option value="-1">--All Taluks--</option>'));
	}

	function showDatatable(dataSet) {
		if (!dataSet || dataSet.length == 0) {
			alert("No results found");
			return;
		}
		var index = 0;
		if (typeof reportsTable == 'undefined') {
			reportsTable = $("#reportsTable").dataTable(
					{
						dom : 'Bfrtip',
						buttons : [ {
							extend : 'csvHtml5',
							title : 'abhiyana_Data'
						}, {
							extend : 'copyHtml5',
						}, {
							extend : 'pdfHtml5',
						}, ],
						pageLength : 25,
						lengthMenu : [ 25, 50, 75, "All" ],
						"destroy" : true,
						"retrieve" : true,
						"processing" : true,
						"deferRender" : true,
						"data" : dataSet,
						"fnRowCallback" : function(nRow, aData, iDisplayIndex) {
							$("td:first", nRow).html(iDisplayIndex + 1);
							return nRow;
						},
						"columns" : [
								{
									"title" : "(1) ಎಸ್ ಸಂಖ್ಯೆ",
									"data" : "talukName",
									"render" : function(datam, type, row) {
										return (++index);
									}

								},
								{
									"title" : "(2) ತಾಲೂಕು",
									"data" : "talukName",
									"render" : function(datam, type, row) {
										return datam
									}

								},
								{
									"title" : "(3) ಹಣಕಾಸು ಟಾರ್ಗೆಟ್",
									"data" : "taluk_fin_target",
									"render" : function(datam, type, row) {
										if (datam == 0) {
											return "NA"
										}
										return datam
									}

								},
								{
									"title" : "(4) ವಾರ್ಷಿಕ ಟಾರ್ಗೆಟ್",
									"data" : "taluk_year_target",
									"render" : function(datam, type, row) {
										if (datam == 0) {
											return "NA"
										}
										return datam
									}

								},
								{
									"title" : "(6) ಸಾಧಿಸಿದ",
									"data" : "achieved",
									"render" : function(datam, type, row) {
										return (row.bhautikeBankLoan + row.bhautikeSubsidy)
												+ (row.arthikeBankLoan + row.arthikeSubsidy);
									}

								}, {
									"title" : "(7) ಶಾರೀರಿಕ ಸಬ್ಸಿಡಿ",
									"data" : "bhautikeSubsidy",
									"render" : function(datam, type, row) {
										return datam
									}
								}, {
									"title" : "(8) ಶಾರೀರಿಕ ಬ್ಯಾಂಕ್ ಸಾಲ",
									"data" : "bhautikeBankLoan",
									"render" : function(datam, type, row) {
										if (datam == 0) {
											return "NA";
										}
										return datam;

									}

								}, {
									"title" : "(9) ಒಟ್ಟು ಸಾಧನೆ",
									"data" : "bhautikeBankLoan",
									"render" : function(datam, type, row) {
										if (datam == 0) {
											return "NA";
										}
										return row.bhautikeBankLoan + row.bhautikeSubsidy;
									}

								}, {
									"title" : "(10) ಆರ್ಥಿಕ ಸಬ್ಸಿಡಿಗಳುಿ",
									"data" : "arthikeSubsidy",
									"render" : function(datam, type, row) {
										return datam
									}
								}, {
									"title" : "(11) ಆರ್ಥಿಕ ಬ್ಯಾಂಕ್ ಸಾಲದ್ಟು",
									"data" : "arthikeBankLoan",
									"render" : function(datam, type, row) {
										/* if (datam == 0) {
											return "NA";
										} */
										return datam;

									}

								}, {
									//arthike total
									"title" : "(12) ಆರ್ಥಿಕ ಒಟ್ಟುೆ",
									"data" : "arthikeBankLoan",
									"render" : function(datam, type, row) {
										/* if (datam == 0) {
											return "NA";
										} */
										return row.arthikeBankLoan + row.arthikeSubsidy;
									}

								}, {
									"title" : "(13) ಮಹಿಳೆಯರು",
									"data" : "women",
									"render" : function(datam, type, row) {
										if (datam == 0) {
											return "NA";
										}
										return datam;
									}
								}

						]
					})

		} else {
			dataTable_managetrips.dataTable().fnClearTable();
			if (dataSet.length != 0) {

				dataTable_managetrips.fnAddData(dataSet)

			}
		}
	}

	function performSearch(form) {
		showLoader();
		var data = {
			request : serializeObject($("#submitForm"))
		}

		console.log(data);

		$.ajax({
			type : "POST",
			contentType : 'application/json; charset=utf-8',
			dataType : 'json',
			url : "dairy/search",
			data : JSON.stringify(data)
		}).done(function(response) {
			hideLoader();
			if (response.code == 200) {
				showDatatable(response.resp);
			} else {
				alert("Could not load data");
			}
			console.log(response);

		})
		return false;
	}
</script>