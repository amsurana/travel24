<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<aside class="main-sidebar">


	<section class="sidebar" style="height: auto;">
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu">
			<li class="header">MAIN NAVIGATION</li>
			<li class="treeview"><a href="dashboard"> <i
					class="fa fa-dashboard"></i> <span>Dashboard</span> <span
					class="pull-right-container"> <i
						class="fa fa-angle-left pull-right"></i>
				</span>
			</a></li>
			<li class="treeview"><a href="#"> <i class="fa fa-files-o"></i>
					<span>Dairy HO</span>
			</a>
				<ul class="treeview-menu">
					<li><a href="${host}/dairyYearlyTargets"> <i
							class="fa fa-circle-o"></i> Head office Form
					</a></li>
					<li><a href="${host}/dairyDistrictWiseReport"> <i
							class="fa fa-circle-o"></i> View District-wise Report
					</a></li>
				</ul></li>

			<li class="treeview"><a href="#"> <i class="fa fa-files-o"></i>
					<span>Dairy District</span>
			</a>
				<ul class="treeview-menu">
					<li><a href="${host}/dairyTalukWiseReport"> <i
							class="fa fa-circle-o"></i>View Taluk-wise Report
					</a></li>
				</ul></li>
			<li class="treeview"><a href="#"> <i class="fa fa-files-o"></i>
					<span>Dairy Taluk</span>
			</a>
				<ul class="treeview-menu">
					<li><a href="${host}/dairyMonthlyReport"> <i
							class="fa fa-circle-o"></i> Fill Monthly Report
					</a></li>
				</ul></li>
			<!-- <c:if test="${pms['WMasterProduct'] eq true}">
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i>
            <span>Manage Products</span>
          </a>
          <ul class="treeview-menu">
            <li>
              <a href="${host}/admin/mfin/product/search">
                <i class="fa fa-circle-o"></i>
                View &amp; Add Products
              </a>
            </li>

          </ul>
        </li>
      </c:if> -->

			<!--  <li class="treeview">
        <a href="${host}/managetrips">
          <i class="fa fa-folder"></i>
          <span> Manage Trips</span>
        </a>
      </li>
 -->

			<!--  <c:if test="${pms['WMasterAgent'] eq true}">
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Manage Agents</span>
          </a>
          <ul class="treeview-menu">
            <li>
              <a href="${host}/admin/agent/search">
                <i class="fa fa-circle-o"></i>
                Search &amp; Add Agents
              </a>
            </li>

            <c:if test="${pms['WTrackAgents'] eq true}">
              <li>
                <a href="${host}/admin/smartracker/devicesList">
                  <i class="fa fa-circle-o"></i>
                  Search &amp; Monitor Agents
                </a>
              </li>
            </c:if>
          </ul>
        </li>
      </c:if> -->

			<!-- <c:if test="${pms['WManageCustomers'] eq true}">
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i>
            <span>Manage Customers</span>
          </a>
          <ul class="treeview-menu">
            <li>
              <a href="${host}/admin/mfin/leads/search-batch">
                <i class="fa fa-circle-o"></i>
                New Leads
              </a>
            </li>
            <li>
              <a href="${host}/admin/mfin/enrollments/search-batch">
                <i class="fa fa-circle-o"></i>
                Customer Records
              </a>
            </li>
          </ul>
        </li>
      </c:if> -->





			<!-- <li>
        <a href="#" onclick="window.open('http://www.smargav.com')">
          <i class="fa fa-circle-o text-aqua"></i>
          <span>FieldX Help</span>
        </a>
      </li> -->
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>


