<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html class="loginPage">
<head>
<meta
    content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
    name='viewport'>
    <title>Login</title>
    <link rel='shortcut icon'
        href='<c:url value="/resources/images/logo.ico" />'
        type='image/x-icon'/ >

        <script type="text/javascript"
            src="<c:url value="/resources/js/jquery-2.1.1.min.js" />"></script>
        <script type="text/javascript" src=" <c:url value="/resources/js/jquery.validate.js" />"></script>
        <link type="text/css" rel="stylesheet"
            href="<c:url value="/resources/theme/bootstrap/css/bootstrap.min.css" />" />
        <link type="text/css" rel="stylesheet"
            href="<c:url value="/resources/theme/css/AdminLTE.css" />" />
        <link type="text/css" rel="stylesheet"
            href="<c:url value="/resources/css/common.css" />" />
        <style>
/* .loginPage {
    background: #083B48;
} */

/* #login-box {
    background: rgb(25, 77, 90);
} */

.header{
    color: rgba(255, 255, 255, 0.5);
    font-size: 1.3em;
    background-color: #07333E;
    margin-bottom: 15px;
}

/* #loginPage #login-box input[type="submit"] {
    background: #F95C00 !important;
}
 */
.product_logo {
    padding: 20px;
    color: rgba(255, 255, 255, 0.5);
    font-size: 40px;
    text-align: center;
}
.product_logo::before{
    content: "powered by ";
    color: rgba(200, 200,200, 0.5);
    font-size:20px;
    vertical-align: center;
}
</style>
</head>

<body id="loginPage" class="">

    <div class="form-box" id="login-box">
        <div class="header">
            <h1>Abhiyana</h1>
        </div>
        <form id='loginForm' action="" name="loginForm">
            <div class="body">
                <div class="form-group">
                    <input type="text" id="username" name="username" class="form-control"
                        placeholder="Employee Id" required data-msg="enter valid username" />
                </div>
                <div class="form-group" style="position: relative">
                    <input type="password" id="password" name="password" class="form-control"
                        placeholder="Password" required data-msg="enter valid password" />
                </div>


            </div>
            <div class="footer">
                <input type="submit" class="btn themeBg btn-block" value="Login" />

            </div>
            <div class="height20"></div>
        </form>
    </div>
</body>

<script type="text/javascript">
$(function (){
    login();
})

function login() {
    $("#loginForm").validate({
        rules: {},
        submitHandler: function(form) {

            var username = $("#username").val();
            var password = $("#password").val();

            var data = {
                "request": {
                    username:username,
                    password:password
                }
            }

              $.ajax({
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                url: "user/login",
                data: JSON.stringify(data)
            }).done(function(response) {

                if(response.code==200){
                    var userData= JSON.stringify(response.resp)
                     localStorage.setItem("userProfile",userData);
                     localStorage.setItem("login_status",1);
                     location.href = "dashboard";
                }else if(response.code==500){
                    alert("Invalid username & password !")
                }

                return
            })
        }
    });

}

</script>

</html>