<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Checkpost</title>
</head>
<body>
     <%@ include file="header.jsp" %>
     <%@ include file="side-menu.jsp" %>
    <div class="content-wrapper" >
        <section class="content">
          <div class="row" style="margin:0px !important;"> 
                  <div class="box  panel box box-warning accessPermission" style="box-shadow: 0px 2px 10px #868282;">
                      <div class="box-header" data-widget="collapse-header">
                          <h3 class="box-title">Add User</h3>
                          <div class="box-tools pull-right">
                              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                          </div>
                      </div><!-- /.box-header -->
                      <div class="box-body">
                         <form id="create_user" name="create_user">
                          <div class="form-group">
                              <h5 class="box-title">*User Name:</h5>
                              <input type="text" class="form-control" id="user_name" name="user_name" required data-msg="Please Enter User Name">
                            </div>
                            <div class="form-group float-right" style="margin-right: 14px;">
                                 <input type="submit" class="btn btn-warning" id="deleteSubmit" value="Add"  />
                            </div>
                        </form>
                      </div><!-- /.box-body -->
              </div><!-- /.box -->
          </div>
          
        </section>
        </div>
    </div>
</body>
</html>
 <script type="text/javascript"
  src="<c:url value="/resources/js/add_users.js" />"></script> 