<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Checkpost</title>
</head>
<body>
     <%@ include file="header.jsp" %>
     <%@ include file="side-menu.jsp" %>
    <div class="content-wrapper" >
        <section class="content">
          <div class="row" style="margin:0px !important;"> 
                  <div class="box  panel box box-warning accessPermission" style="box-shadow: 0px 2px 10px #868282;">
                      <div class="box-header" data-widget="collapse-header">
                          <h3 class="box-title">Change Password</h3>
                          <div class="box-tools pull-right">
                              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                          </div>
                      </div><!-- /.box-header -->
                      <div class="box-body">
                         <form id="create_new_password" name="create_new_password">
                          	<div class="form-group">
	                              <h5 class="box-title">*Current Password:</h5>
	                              <input type="text" class="form-control" id="curr-pass" required data-msg="Please Enter Current Password">
                            </div>
                            <div class="form-group">
	                              <h5 class="box-title">*New Password:</h5>
	                              <input type="password" class="form-control" id="new_pass" name="new_pass" required data-msg="Please Enter New Password">
                            </div>
                            <div class="form-group">
	                              <h5 class="box-title">*Retype-Password:</h5>
	                              <input type="password" class="form-control" id="re-new-name" name="re-new-name" required data-msg="Please Enter Retype New Password">
                            </div>
                            <div class="form-group " style="margin-left: 48%; margin-top: 3%;">
                                 <input type="submit" class="btn btn-warning" id="submitButton" value="Submit"  />
                            </div>
                        </form>
                      </div><!-- /.box-body -->
              </div><!-- /.box -->
          </div>
        </section>
        </div>
    </div>
</body>
</html>
 <script type="text/javascript"
  src="<c:url value="/resources/js/changePassword.js" />"></script> 