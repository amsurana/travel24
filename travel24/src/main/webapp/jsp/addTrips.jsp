<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Distance & Duration </title>
</head>
<body>
     <%@ include file="header.jsp" %>
     <%@ include file="side-menu.jsp" %>
    <div class="content-wrapper" >
        <section class="content">
         <div class="row" style="margin:0px !important;"> 
            <div class="box  panel box box-warning accessPermission" style="box-shadow: 0px 2px 10px #868282;">
                <div class="box-header" data-widget="collapse-header">
                    <h3 class="box-title">Distance & Duration (Checkpost -> Checkpost)</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <form id="create_trip_form" name="create_trip_form">  
                    <div class="form-group col-md-3">
                      <label for="cp_list_from">*From:</label>
                      <select class="form-control" id="cp_list_from" required data-msg='Please Select Source Point'>
                          <option value="">--Select Checkpoint--</option>
                      </select>
                    </div>
      
                    <div class="form-group col-md-3">
                      <label for="cp_list-to">*To:</label>
                        <select class="form-control" id="cp_list_to" name="cp_list_to"  required data-msg='Please Select Distination'>
                            <option value="">--Select Checkpoint--</option>
                        </select>
                    </div> 

                    <div class="form-group col-md-3">
                      <label for="trip_distance">*Distance (in km):</label>
                      <input type="number" min="0" oninput="validity.valid||(value='');" class="form-control" name="trip_distance" id="trip_distance" required data-msg='Please Enter Distance'>
                    </div> 

                    <div class="form-group col-md-3">
                      <label for="trip_duration">*Duration (in min):</label>
                      <input type="number" min="0" oninput="validity.valid||(value='');" class="form-control" name="trip_duration" id="trip_duration" required data-msg='Please Enter Journey Duration'>
                    </div>  
                    <div class="form-group float-right" style="margin-right: 14px;">
                      <input type="submit" class="btn btn-warning" value="add " />
                    </div>           
                  </form>
                </div>
              </div><!-- /.box -->
          </div>
          <div class="row">
             <div class="col-md-12">
                <div class="height10"></div>
                <div class=" tableSection sectionBgWhite ">
                   <div class="height20"></div>
                   <table id="tripsTable" class="stripe table table-bordered table-striped" style="width:100%">
                   </table>
                </div>
             </div>

          </div>
      </section>
    </div>
</body>
</html>
<script type="text/javascript"
  src="<c:url value="/resources/js/add_Trips.js" />"></script>