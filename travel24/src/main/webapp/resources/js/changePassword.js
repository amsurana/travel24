$(function() {

    changePassWord();
})

function changePassWord() {

    $("#create_new_password").validate({

          rules: {},
        submitHandler: function(form) {
            showLoader();
            var userData = jQuery.parseJSON(localStorage.getItem("userProfile"));
            var newPassword=$("#new_pass").val();
            var currentPassword=$("#curr-pass").val();
            var data = {
                "request": {
                        name:userData.name,
                        "currentPassword":currentPassword,
                        "newPassword":newPassword
                }
            }
            $.ajax({
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                url: "user/change_password",
                data: JSON.stringify(data)
            }).done(function(response) {
                hideLoader();
                if(response.code==200){
                     alert("Successfully changed");
                    location.reload();
                }else{
                    alert("Oops Something Went Wrong!")
                }
              

            })
            return false;
        }
    });

}