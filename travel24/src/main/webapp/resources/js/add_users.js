$(function(){
	regUser();
})

function regUser() {

    $("#create_user").validate({

          rules: {},
        submitHandler: function(form) {
            showLoader();
            var userName=$("#user_name").val();
            var data = {
                "request": {
                        name:userName,
                }
            }
            $.ajax({
                type: "POST",
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                url: "user/add",
                data: JSON.stringify(data)
            }).done(function(response) {
                hideLoader();
                if(response.code==200){
                     alert("Successfully Added !");
                    location.reload();
                }else{
                    alert("Oops Something Went Wrong!")
                }
              

            })
            return false;
        }
    });

}