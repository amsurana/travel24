package com.travel24.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserViewController {
      	
	  @RequestMapping(value="dashboard",method=RequestMethod.GET)
	  public ModelAndView getDashboard(){
		  return new ModelAndView("dashboard");
	  }
	  
	  
	  @RequestMapping(value="trips",method=RequestMethod.GET)
	  public ModelAndView getTrip(){
		  return new ModelAndView("addTrips");
	  }
	  @RequestMapping(value="chekposts",method=RequestMethod.GET)
	  public ModelAndView getChekposts(){
		  return new ModelAndView("addChekposts");
	  }
	  
	  @RequestMapping(value="/",method=RequestMethod.GET)
	  public ModelAndView getIndex(){
		  return new ModelAndView("login");
	  }
	  
	  @RequestMapping(value="changepassword",method=RequestMethod.GET)
	  public ModelAndView getChangePassword(){
		  return new ModelAndView("changePassword");
	  }
	  
	  @RequestMapping(value="newregistration",method=RequestMethod.GET)
	  public ModelAndView getNewRegistration(){
		  return new ModelAndView("newRegistration");
	  }
	  
	  
}