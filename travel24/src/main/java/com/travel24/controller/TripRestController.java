package com.travel24.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.travel24.customapi.CustomApi;
import com.travel24.model.TripDetail;
import com.travel24.model.Vehicle;
import com.travel24.repo.CheckPostRepo;
import com.travel24.repo.TripRepo;
import com.travel24.repo.VehicleRepo;
import com.travel24.rest.Request;
import com.travel24.rest.Response;
import com.travel24.rest.ResponseCodes;
import com.travel24.rest.TripDetailSearchReq;

@RestController
@RequestMapping("/trip")
public class TripRestController {

	@Autowired
	private TripRepo tripRepo;

	@Autowired
	private VehicleRepo vehicleRepo;

	@Autowired
	private CheckPostRepo checkPostRepo;

	@Autowired
	private CustomApi customeApi;

	private static final Logger logs = Logger.getLogger(UserRestController.class);

	@RequestMapping(value = "byendcp", method = RequestMethod.GET)
	public Response getByEndCps(@RequestParam("id") long endCp) {
		Response resp = new Response();
		List<TripDetail> tripDetails;
		try {
			tripDetails = tripRepo.findByEndCp(endCp);
			resp.setResp(tripDetails);
			resp.setCode(ResponseCodes.SUCCESS);
			resp.setMessage(ResponseCodes.SUCCESS_MSG);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setResp(e.getMessage());
			resp.setCode(ResponseCodes.FAILURE);
			resp.setMessage(ResponseCodes.FAILURE_MSG);
		}

		return resp;
	}

	@RequestMapping(value = "byendcptime", method = RequestMethod.GET)
	public Response getByEndCpsTime(@RequestParam("id") long endCp, @RequestParam("time") long entryTime) {
		Response resp = new Response();
		List<TripDetail> tripDetails;
		try {
			tripDetails = tripRepo.findByEndCpAndEntryTime(endCp, entryTime);
			resp.setResp(tripDetails);
			resp.setCode(ResponseCodes.SUCCESS);
			resp.setMessage(ResponseCodes.SUCCESS_MSG);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resp.setResp(e.getMessage());
			resp.setCode(ResponseCodes.FAILURE);
			resp.setMessage(ResponseCodes.FAILURE_MSG);
		}

		return resp;
	}

	@RequestMapping(value = "add", method = RequestMethod.POST)
	public Response addtrip(@RequestBody Request<TripDetail> req) {
		Response resp = new Response();
		try {

			TripDetail t = req.getRequest();

			if (t == null) {
				resp.setResp("NULL_REQUEST_SENT");
				resp.setCode(ResponseCodes.FAILURE);
				resp.setMessage(ResponseCodes.FAILURE_MSG);
				return resp;
			}

			if (t.getId() != 0) {
				return update(t);
			} else {
				return insert(t);
			}

		} catch (Exception e) {
			resp.setResp(e.getMessage());
			resp.setCode(ResponseCodes.FAILURE);
			resp.setMessage(ResponseCodes.FAILURE_MSG);
		}
		logs.info(resp.toJson());
		System.out.println(resp.toJson());
		return resp;
	}

	/*@RequestMapping(value="all",method=RequestMethod.GET)
	public Response getTripDetails(){
        Response resp=new Response();
     
        try {
           
        	ArrayList<TripDetail> allTrips=new ArrayList<TripDetail>();
        	List<TripDetail> trips=tripRepo.findAll();
            for(TripDetail tripdetail:trips){
               tripdetail.setFromCheckPost(checkPostRepo.findOne(tripdetail.getStartCp()));
               tripdetail.setEndCheckPost(checkPostRepo.findOne(tripdetail.getEndCp()));
               tripdetail.setVehicleDetails(vehicleRepo.findOne(tripdetail.getVehicleId()));
               allTrips.add(tripdetail);
 
            }
            
        }*/
	private Response insert(TripDetail tripDetail) {
		Response resp = new Response();

		// int
		// vehicleId=vehicleDao.getVehicleByName(req.getRequest().getVehicleNo());
		Vehicle vehicle = vehicleRepo.findByNumber(tripDetail.getVehicleNo());
		if (vehicle != null) {
			tripDetail.setVehicleId(vehicle.getId());
		} else {
			Vehicle addVehicle = new Vehicle();
			addVehicle.setType(tripDetail.getVehicleType());
			addVehicle.setNumber(tripDetail.getVehicleNo());
			addVehicle.setCreatedTime(System.currentTimeMillis());
			vehicleRepo.save(addVehicle);
			tripDetail.setVehicleId(addVehicle.getId());
		}

		tripDetail.setCreatedTime(System.currentTimeMillis());

		tripRepo.save(tripDetail);
		resp.setResp(tripDetail.getId());
		resp.setCode(ResponseCodes.SUCCESS);
		resp.setMessage(ResponseCodes.SUCCESS_MSG);

		return resp;
	}

	private Response update(TripDetail t) {
		Response resp = new Response();

		// We update only Delay, Over, Remarks field, Exit time.
		TripDetail tripDetail = tripRepo.findOne(t.getId());
		tripDetail.setOver(t.getOver());
		tripDetail.setDelay(t.getDelay());
		tripDetail.setRemark(t.getRemark());
		tripDetail.setExitTime(t.getExitTime());

		tripRepo.save(tripDetail);

		resp.setResp(tripDetail.getId());
		resp.setCode(ResponseCodes.SUCCESS);
		resp.setMessage(ResponseCodes.SUCCESS_MSG);

		return resp;
	}

	@RequestMapping(value = "all", method = RequestMethod.GET)
	public Response getTripDetails() {
		Response resp = new Response();

		try {

			ArrayList<TripDetail> allTrips = new ArrayList<TripDetail>();
			List<TripDetail> trips = tripRepo.findAll();
			for (TripDetail tripdetail : trips) {
				tripdetail.setFromCheckPost(checkPostRepo.findOne(tripdetail.getStartCp()));
				tripdetail.setEndCheckPost(checkPostRepo.findOne(tripdetail.getEndCp()));
				tripdetail.setVehicleDetails(vehicleRepo.findOne(tripdetail.getVehicleId()));
				allTrips.add(tripdetail);
			}
			resp.setResp(allTrips);
			resp.setCode(ResponseCodes.SUCCESS);
			resp.setMessage(ResponseCodes.SUCCESS_MSG);
		} catch (Exception e) {
			resp.setResp(e.getMessage());
			resp.setCode(ResponseCodes.FAILURE);
			resp.setMessage(ResponseCodes.FAILURE_MSG);
		}
		logs.info(resp.toJson());
		System.out.println(resp.toJson());
		return resp;
	}

	@RequestMapping(value = "update", method = RequestMethod.POST)
	public Response updateTrip(@RequestBody Request<TripDetail> req) {
		Response resp = new Response();
		try {
			tripRepo.save(req.getRequest());
			resp.setMessage(ResponseCodes.SUCCESS_MSG);
			resp.setResp(null);
			resp.setCode(ResponseCodes.SUCCESS);
		} catch (Exception e) {
			resp.setResp(e.getMessage());
			resp.setCode(ResponseCodes.SUCCESS);
			resp.setMessage(ResponseCodes.FAILURE_MSG);
		}
		logs.info(resp.toJson());
		System.out.println(resp.toJson());
		return resp;
	}

	@RequestMapping(value = "search", method = RequestMethod.POST)
	public Response searchTrip(@RequestBody Request<TripDetailSearchReq> req) {
		Response resp = new Response();

		try {
			ArrayList<TripDetail> listOfTrips = new ArrayList<TripDetail>();
			List<TripDetail> trips = customeApi.searchTrips(req.getRequest());

			for (TripDetail trip : trips) {
				trip.setFromCheckPost(checkPostRepo.findOne(trip.getStartCp()));
				trip.setEndCheckPost(checkPostRepo.findOne(trip.getEndCp()));
				trip.setVehicleDetails(vehicleRepo.findOne(trip.getVehicleId()));
				listOfTrips.add(trip);
			}

			resp.setResp(listOfTrips);
			resp.setMessage(ResponseCodes.SUCCESS_MSG);
			resp.setCode(ResponseCodes.SUCCESS);
		} catch (Exception e) {
			resp.setResp(e.getMessage());
			resp.setCode(ResponseCodes.FAILURE);
			resp.setMessage(ResponseCodes.FAILURE_MSG);
		}
		logs.info(resp.toJson());
		System.out.println(resp.toJson());
		return resp;
	}

	@RequestMapping(value = "delete", method = RequestMethod.GET)
	public Response deleteTrip(@RequestParam("id") long id) {
		Response resp = new Response();
		if (id != 0) {
			try {
				tripRepo.delete(id);
				resp.setCode(ResponseCodes.SUCCESS);
				resp.setMessage(ResponseCodes.SUCCESS_MSG);
				resp.setResp(null);
			} catch (Exception e) {
				e.printStackTrace();
				resp.setCode(ResponseCodes.FAILURE);
				resp.setMessage(ResponseCodes.FAILURE_MSG);
				resp.setResp(e.getMessage());
			}
		}
		return resp;
	}

}
