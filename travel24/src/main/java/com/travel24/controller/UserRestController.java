package com.travel24.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.travel24.model.User;
import com.travel24.repo.CheckPostRepo;
import com.travel24.repo.UserRepo;
import com.travel24.rest.Apis;
import com.travel24.rest.ChangePassword;
import com.travel24.rest.Request;
import com.travel24.rest.Response;
import com.travel24.rest.ResponseCodes;


@RestController
@RequestMapping(value="user")
public class UserRestController {
    
	
	@Autowired
	private UserRepo userRepo;
	
	@Autowired
	private CheckPostRepo checkPostRepo;
	
	
	
	private static final Logger logs=Logger.getLogger(UserRestController.class);
	
	
	@RequestMapping(value="login",method=RequestMethod.POST)
	public Response userLogin(@RequestBody Request<User> req){
        Response resp=new Response();
        try {
        	BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();	
            User usercheck=userRepo.findByName(req.getRequest().getName());
	        if(usercheck!=null && passwordEncoder.matches(req.getRequest().getPassword(), usercheck.getPassword())){
	        	resp.setResp(usercheck);
				resp.setCode(ResponseCodes.SUCCESS);
				resp.setMessage(ResponseCodes.SUCCESS_MSG);
	        }else{
	        	resp.setResp(null);
				resp.setCode(ResponseCodes.FAILURE);
				resp.setMessage(ResponseCodes.FAILURE_MSG);
	        }
	        } catch (Exception e) {
				resp.setResp(e.getMessage());
				resp.setCode(ResponseCodes.FAILURE);
				resp.setMessage(ResponseCodes.FAILURE_MSG);
			}
	        logs.info(resp.toJson());
	        System.out.println(resp.toJson());
			return resp;
        }
	
	
	@RequestMapping(value="change_password",method=RequestMethod.POST)
	public Response changePassword(@RequestBody Request<ChangePassword> req){
        Response resp=new Response();
        try {
        	BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();	
            User usercheck=userRepo.findByName(req.getRequest().getName());
	        if(usercheck!=null && passwordEncoder.matches(req.getRequest().getCurrentPassword(), usercheck.getPassword())){
	        	usercheck.setPassword(passwordEncoder.encode(req.getRequest().getNewPassword()));
	        	userRepo.save(usercheck);
	        	resp.setResp(usercheck);
				resp.setCode(ResponseCodes.SUCCESS);
				resp.setMessage(ResponseCodes.SUCCESS_MSG);
	        }else{
	        	resp.setResp(null);
				resp.setCode(ResponseCodes.FAILURE);
				resp.setMessage(ResponseCodes.FAILURE_MSG);
	        }
	        } catch (Exception e) {
				resp.setResp(e.getMessage());
				resp.setCode(ResponseCodes.FAILURE);
				resp.setMessage(ResponseCodes.FAILURE_MSG);
			}
	        logs.info(resp.toJson());
	        System.out.println(resp.toJson());
			return resp;
        }
        	
	
        	
	@RequestMapping(value="add",method=RequestMethod.POST)
	public Response getAllUserDetails(@RequestBody Request<User> req){
        Response resp=new Response();
        try {
        	User user=req.getRequest();
        	if(user!=null){
            		
        	BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();		
            user.setPassword(passwordEncoder.encode("admin"));		
        	user.setCreatedTime(System.currentTimeMillis());	
			userRepo.save(user);
        	resp.setResp(null);
			resp.setCode(ResponseCodes.SUCCESS);
			resp.setMessage(ResponseCodes.SUCCESS_MSG);
        	}else{
        		resp.setResp("NULL_REQUEST_SENT");
    			resp.setCode(ResponseCodes.FAILURE);
    			resp.setMessage(ResponseCodes.FAILURE_MSG);
        	}
		} catch (Exception e) {
			e.printStackTrace();
			resp.setResp(e.getMessage());
			resp.setCode(ResponseCodes.FAILURE);
			resp.setMessage(ResponseCodes.FAILURE_MSG);
		}
        logs.info(resp.toJson());
        System.out.println(resp.toJson());
		return resp;
	}
	
	
	
	
	@RequestMapping(value=Apis.GET_ALL_USER,method=RequestMethod.GET)
	public Response getAllPersonDetails(){
        Response resp=new Response();
        try {
			
            ArrayList<User> users=new ArrayList<User>();
            List<User> getAllUsers=userRepo.findAll();
            for(User user :getAllUsers){
                user.setCheckPost(checkPostRepo.findOne(user.getCheckPostId()));
                users.add(user);
            }
			resp.setResp(users);
			resp.setCode(ResponseCodes.SUCCESS);
			resp.setMessage(ResponseCodes.SUCCESS_MSG);
		} catch (Exception e) {
			resp.setResp(e.getMessage());
			resp.setCode(ResponseCodes.FAILURE);
			resp.setMessage(ResponseCodes.FAILURE_MSG);
		}
        logs.info(resp.toJson());
        System.out.println(resp.toJson());
		return resp;
	}
	

	
}
