package com.travel24.rest;

public class VehicleStatsInfo {


	public VehicleStatsInfo() {
		super();
		// TODO Auto-generated constructor stub
	}
	private String vehicleName;
	private int over,delay;
	
	public String getVehicleName() {
		return vehicleName;
	}
	public void setVehicleName(String vehicleName) {
		this.vehicleName = vehicleName;
	}
	public int getOver() {
		return over;
	}
	public void setOver(int over) {
		this.over = over;
	}
	public int getDelay() {
		return delay;
	}
	public void setDelay(int delay) {
		this.delay = delay;
	}
	
}
