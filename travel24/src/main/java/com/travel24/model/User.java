package com.travel24.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.travel24.repo.CheckPostRepo;


/**
 * The persistent class for the users database table.
 * 
 */
@Entity
@Table(name="users")
@NamedQueries({
	@NamedQuery(name="User.findAll", query="SELECT u FROM User u"),
	@NamedQuery(name="User.getByToCp", query="SELECT u FROM User u")
})

public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	@Column(name="created_time")
	private long createdTime;

	private String name;

	private String password;


	@Transient
    private CheckPost checkPost;
	
	//bi-directional one-to-one association to CheckPost
	//@OneToOne
	@Column(name="cp_details")
	private long checkPostId;

	public User() {
	
	}
   
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(long createdTime) {
		this.createdTime = createdTime;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return this.password;
	}

	public CheckPost getCheckPost() {
		return checkPost;
	}

	public void setCheckPost(CheckPost checkPost) {
		this.checkPost = checkPost;
	}


	public long getCheckPostId() {
		return checkPostId;
	}

	public void setCheckPostId(long checkPostId) {
		this.checkPostId = checkPostId;
	}


	public void setPassword(String password) {
		this.password = password;
	}
	

	
}