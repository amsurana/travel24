package com.smargav.travel24.db.dbmodel;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.smargav.travel24.db.TripDetailsDao;

import java.io.Serializable;


/**
 * The persistent class for the trip_details database table.
 */
@DatabaseTable(daoClass = TripDetailsDao.class)
public class TripDetail implements Serializable {
    private static final long serialVersionUID = 1L;

    @DatabaseField
    private long id;

    @DatabaseField
    private int delay;

    @DatabaseField
    private String driverName;

    @DatabaseField
    private long endCp;

    @DatabaseField
    private long entryTime;

    @DatabaseField
    private long exitTime;

    @DatabaseField
    private long numberOfPassenger;

    @DatabaseField
    private int over;

    @DatabaseField
    private String remark;

    @DatabaseField
    private long startCp;


    //Vehicle number is always unique!
    @DatabaseField(id = true)
    private String vehicleNo;

    @DatabaseField
    private String vehicleType;


    @DatabaseField
    private boolean synced;

    @DatabaseField
    private boolean fromNet;

    private Vehicle vehicleDetails;

    @DatabaseField
    private boolean completed;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public long getEndCp() {
        return endCp;
    }

    public void setEndCp(long endCp) {
        this.endCp = endCp;
    }

    public long getEntryTime() {
        return entryTime;
    }

    public void setEntryTime(long entryTime) {
        this.entryTime = entryTime;
    }

    public long getExitTime() {
        return exitTime;
    }

    public void setExitTime(long exitTime) {
        this.exitTime = exitTime;
    }


    public long getNumberOfPassenger() {
        return numberOfPassenger;
    }

    public void setNumberOfPassenger(long numberOfPassenger) {
        this.numberOfPassenger = numberOfPassenger;
    }


    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public long getStartCp() {
        return startCp;
    }

    public void setStartCp(long startCp) {
        this.startCp = startCp;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public boolean isFromNet() {
        return fromNet;
    }

    public void setFromNet(boolean fromNet) {
        this.fromNet = fromNet;
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    public int getOver() {
        return over;
    }

    public void setOver(int over) {
        this.over = over;
    }

    public Vehicle getVehicleDetails() {
        return vehicleDetails;
    }

    public void setVehicleDetails(Vehicle vehicleDetails) {
        this.vehicleDetails = vehicleDetails;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public TripDetail copy() {
        TripDetail d = new TripDetail();
        d.vehicleNo = vehicleNo;
        d.vehicleType = vehicleType;
        d.delay = delay;
        d.endCp = endCp;
        d.entryTime = entryTime;
        d.exitTime = exitTime;
        d.numberOfPassenger = numberOfPassenger;
        d.startCp = startCp;
        d.over = over;
        return d;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TripDetail detail = (TripDetail) o;

        return vehicleNo.equals(detail.vehicleNo);

    }

    @Override
    public int hashCode() {
        return vehicleNo.hashCode();
    }
}