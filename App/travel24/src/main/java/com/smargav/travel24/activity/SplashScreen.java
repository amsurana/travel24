package com.smargav.travel24.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.smargav.travel24.bg.SyncService;
import com.smargav.travel24.db.NetResponse;
import com.smargav.travel24.db.dbmodel.Checkpost;
import com.smargav.travel24.utils.APIUtils;
import com.smargav.travel24.utils.AppConstants;
import com.smargav.travel24.utils.PreferencesUtil;
import com.smargav.travel24.utils.ProgressAsyncTask;
import com.smargav.travel24.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Response;

/**
 * Created by Amit S on 02/09/16.
 */
public class SplashScreen extends AppCompatActivity {

    List<Checkpost> checkposts = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!PreferencesUtil.contains(this, AppConstants.DEF_CP)) {
            new CheckPostDownloaderTask(this).execute();
        } else {
            loadNext();
        }


    }

    private class CheckPostDownloaderTask extends ProgressAsyncTask<Void, Integer> {

        public CheckPostDownloaderTask(Context ctx) {
            super(ctx);
        }

        @Override
        public void onPostExecute(Integer result) {
            super.onPostExecute(result);
            if (result == FAILED) {
                Utils.showPrompt(SplashScreen.this, "Erorr", "Error occured while fetching data. Please try again", true);
            } else if (result == SUCCESS) {
                showCheckPostList();
            }
        }

        @Override
        protected Integer doInBackground(Void... params) {
            try {

                if (!Utils.hasNetwork(ctx)) {
                    return NO_NETWORK;
                }

                //Get All Cps
                Call<NetResponse<List<Checkpost>>> call = APIUtils.api.getCheckposts();
                Response<NetResponse<List<Checkpost>>> resp = call.execute();
                NetResponse<List<Checkpost>> body = resp.body();
                checkposts = body.getResp();


                if (checkposts == null || checkposts.isEmpty()) {
                    return FAILED;
                }

                PreferencesUtil.put(ctx, AppConstants.CHECKPOSTS, checkposts);
                APIUtils.downloadCpDistances(getApplicationContext());

                return SUCCESS;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return FAILED;
        }
    }

    private void showCheckPostList() {

        CharSequence[] cps = new CharSequence[checkposts.size()];

        for (int i = 0; i < checkposts.size(); i++) {
            cps[i] = checkposts.get(i).getName();
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Select default checkpost");
        builder.setItems(cps, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                confirm(checkposts.get(which));
            }
        }).create().show();
    }

    private void confirm(final Checkpost checkpost) {
        Utils.showPrompt(this, "Confirm", "Are you sure to select - " + checkpost.getName(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE) {
                    PreferencesUtil.put(SplashScreen.this, AppConstants.DEF_CP, checkpost);
                    loadNext();
                } else {
                    //finish();
                    showCheckPostList();
                }
            }
        }, new String[]{"OK", "Cancel"});
    }

    private void loadNext() {
        Intent intent = new Intent(this, Home.class);
        startActivity(intent);
        finish();

        SyncService.startSync(this);
        APIUtils.downloadCpDistances(getApplicationContext());
    }


}
