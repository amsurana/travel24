package com.smargav.travel24.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.smargav.travel24.R;

/**
 * Created by smargav on 26/8/16.
 */
public class LoginScreen extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);
        final EditText mUserName = (EditText)findViewById(R.id.usernameETID);
        final EditText mPassword =(EditText)findViewById(R.id.passwordETID);



        Button submitLogin = (Button)findViewById(R.id.loginBTNID);
        submitLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent loginIntent = new Intent(LoginScreen.this,Home.class);
                       startActivity(loginIntent);




            }
        });
    }
}
