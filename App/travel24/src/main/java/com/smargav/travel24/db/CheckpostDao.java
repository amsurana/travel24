package com.smargav.travel24.db;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.smargav.travel24.db.dbmodel.Checkpost;

import java.sql.SQLException;

@SuppressWarnings("unchecked")
public class CheckpostDao extends BaseDaoImpl<Checkpost, String> {


    public CheckpostDao(ConnectionSource connectionSource, Class<Checkpost> dataClass)
            throws SQLException {
        super(connectionSource, dataClass);
    }


}
