package com.smargav.travel24.utils;

/**
 * Created by Amit S on 02/09/16.
 */
public class AppConstants {

    public static final String CHECKPOSTS = "cpKey";
    public static final String CP_DISTANCES = "cpDistances";
    public static final String DEF_CP = "defaultCp";
}
