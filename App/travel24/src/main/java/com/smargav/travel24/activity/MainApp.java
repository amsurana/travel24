package com.smargav.travel24.activity;

import android.app.Application;

import com.smargav.travel24.db.TransactionDBManager;

/**
 * Created by Amit S on 02/09/16.
 */
public class MainApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        TransactionDBManager.init(this);
    }
}
