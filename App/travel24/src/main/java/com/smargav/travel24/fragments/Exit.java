package com.smargav.travel24.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.smargav.travel24.R;
import com.smargav.travel24.db.NetResponse;
import com.smargav.travel24.db.TransactionDBManager;
import com.smargav.travel24.db.TripDetailsDao;
import com.smargav.travel24.db.dbmodel.Checkpost;
import com.smargav.travel24.db.dbmodel.CpDistance;
import com.smargav.travel24.db.dbmodel.TripDetail;
import com.smargav.travel24.utils.APIUtils;
import com.smargav.travel24.utils.AppConstants;
import com.smargav.travel24.utils.PreferencesUtil;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;


public class Exit extends Fragment {

    private EditText vehicleNumber;
    private ListView mylistview;
    private View rootView;

    private Set<TripDetail> exitListView = new HashSet<>();

    private VehiclesListAdapter adapter;


    private List<CpDistance> cpDistances;

    public static Map<Long, Map<Long, CpDistance>> map = new HashMap<>();

    private static Map<String, Integer> vehicleTypeDelayMap = new HashMap<>();


    private Activity mActivity;


    public Exit() {
    }

    @Override
    public void onAttach(Activity activity) {
        mActivity = activity;
        super.onAttach(activity);
    }

    @Override
    public void onAttach(Context context) {
        mActivity = (Activity) context;
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_exit, container, false);

        mylistview = (ListView) rootView.findViewById(R.id.vehicleList);

        //setupFragment();

        preProcessCpDistance();

        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        setupFragment();
    }

    private void preProcessCpDistance() {
        cpDistances = PreferencesUtil.get(getmActivity(), AppConstants.CP_DISTANCES, new TypeToken<List<CpDistance>>() {
        }.getType());

        if (cpDistances == null) {
            return;
        }
        for (CpDistance distance : cpDistances) {
            Map<Long, CpDistance> m = map.get(distance.getFromCp());
            if (m == null) {
                m = new HashMap<>();
                map.put(distance.getFromCp(), m);
            }
            m.put(distance.getToCp(), distance);
        }

        String[] vehicles = getResources().getStringArray(R.array.vehicle_type);
        int[] delay = getResources().getIntArray(R.array.vehicle_delay);

        for (int i = 0; i < vehicles.length; i++) {
            vehicleTypeDelayMap.put(vehicles[i], delay[i]);
        }
    }

    private void setupFragment() {
        exitListView.clear();
        //if (Utils.hasNetwork(getActivity())) {
        loadExitVehicles();
//        } else {
//            loadFromLocal();
//        }
    }

    private void loadExitVehicles() {

        Call<NetResponse<List<TripDetail>>> callExit = APIUtils.search(PreferencesUtil.get(getmActivity(), AppConstants.DEF_CP, Checkpost.class).getId());
        callExit.enqueue(new Callback<NetResponse<List<TripDetail>>>() {
            @Override
            public void onResponse(Response<NetResponse<List<TripDetail>>> response, Retrofit retrofit) {
                //exitListView.addAll();
                saveList(response.body().getResp());
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("ExitFrag", t.getMessage(), t);
                loadFromLocal();

            }
        });
    }

    private void loadFromLocal() {

        try {
            TripDetailsDao dao = TransactionDBManager.getInstance().getEntryDetailsDAO();
            List<TripDetail> local = dao.getAllFromNet();
            if (local != null) {
                exitListView.addAll(local);
            }

            if (!exitListView.isEmpty()) {
                showListView();
                return;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        showEmptyView();

    }

    private void saveList(List<TripDetail> listFromNet) {

        try {

            if (listFromNet == null) {
                loadFromLocal();
                return;
            }

            TripDetailsDao dao = TransactionDBManager.getInstance().getEntryDetailsDAO();

            Iterator<TripDetail> it = listFromNet.iterator();
            while (it.hasNext()) {
                TripDetail detail = it.next();

                detail.setVehicleType(detail.getVehicleDetails().getType());
                detail.setVehicleNo(detail.getVehicleDetails().getNumber());

                TripDetail saved = dao.queryForSameId(detail);
                if (saved != null && saved.getExitTime() != 0) {
                    it.remove();
                    continue;
                }

                detail.setVehicleType(detail.getVehicleDetails().getType());
                detail.setVehicleNo(detail.getVehicleDetails().getNumber());
                //We set it to synced by default. We flip status once details are updated.
                detail.setSynced(true);
                detail.setFromNet(true);

                dao.createOrUpdate(detail);
            }

            this.exitListView.clear();
            this.exitListView.addAll(listFromNet);

            //We combine with local.
            loadFromLocal();
            return;
//            if (listFromNet != null && !listFromNet.isEmpty()) {
//                showListView();
//                return;
//            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        showEmptyView();
    }


    public void showListView() {
        rootView.findViewById(R.id.empty).setVisibility(View.GONE);
        mylistview.setVisibility(View.VISIBLE);
        adapter = new VehiclesListAdapter(getmActivity(), new ArrayList<>(exitListView));
        mylistview.setAdapter(adapter);

        mylistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showExitCompleteDialog((TripDetail) view.getTag());
            }
        });
    }


    private void showEmptyView() {
        rootView.findViewById(R.id.empty).setVisibility(View.VISIBLE);
        mylistview.setVisibility(View.GONE);
    }

    private String formatDate(long entryTime) {

//        Date d = new Date(entryTime);
//        Calendar c = Calendar.getInstance();
//        c.setTime(d);
//        return c.get(Calendar.DATE) + "/" + c.get(Calendar.MONTH) + "/" + c.get(Calendar.YEAR);
        DateTimeFormatter format = DateTimeFormat.forPattern("dd/MM/yy HH:mm");
        return format.print(entryTime);

    }

    public void downloadExitPoints() {
        //if (Utils.hasNetwork(getActivity())) {
        Toast.makeText(getmActivity(), "Refreshing exit points, Please wait...", Toast.LENGTH_SHORT).show();
        setupFragment();
//        } else {
//            Utils.showPrompt(getmActivity(), "Error", "No internet. Please try again.");
//        }
    }

    private void showExitCompleteDialog(final TripDetail tag) {

        TripCompletionDialog dialog = new TripCompletionDialog();
        dialog.setTripDetail(tag);

//        dialog.onDismiss(new DialogInterface() {
//            @Override
//            public void cancel() {
//
//            }
//
//            @Override
//            public void dismiss() {
//                loadFromLocal();
//            }
//        });

        dialog.show(getFragmentManager(), null);
    }


    //***************************************************************************

    private class VehiclesListAdapter extends BaseAdapter {

        public static final int MINUTE = 60 * 1000;
        private Context ctx;
        private List<TripDetail> details;

        public VehiclesListAdapter(Context context, List<TripDetail> details) {
            this.ctx = context;
            this.details = details;
        }

        @Override
        public int getCount() {
            return details.size();
        }

        @Override
        public Object getItem(int position) {
            return details.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TripDetail detail = (TripDetail) getItem(position);

            if (convertView == null) {
                convertView = View.inflate(ctx, R.layout.list_item, null);
            }

            applyAppropriateBackground(convertView, detail);


            ((TextView) convertView.findViewById(R.id.vehicle_number)).setText(detail.getVehicleNo());
            ((TextView) convertView.findViewById(R.id.driver_name)).setText(detail.getDriverName());
            ((TextView) convertView.findViewById(R.id.time)).setText(formatDate(detail.getEntryTime()));

            convertView.setTag(detail);

            return convertView;
        }

        private void applyAppropriateBackground(View convertView, TripDetail detail) {

            Map<Long, CpDistance> m = map.get(detail.getStartCp());
            if (m == null) {
                convertView.setBackgroundColor(Color.TRANSPARENT);
                return;
            }

            CpDistance distance = m.get(detail.getEndCp());

            if (distance == null) {
                convertView.setBackgroundColor(Color.TRANSPARENT);
                return;
            }


            Integer delay = vehicleTypeDelayMap.get(detail.getVehicleType());
            if (delay == null) {
                delay = 10;
            }
            //Delay.
            if ((detail.getEntryTime() + distance.getTimeTaken() * MINUTE + delay * MINUTE) < System.currentTimeMillis()) {
                convertView.setBackgroundResource(R.color.delay_color);
            } else {
                convertView.setBackgroundColor(Color.TRANSPARENT);
            }
            //overspeed
//            else if ((detail.getEntryTime() + distance.getTimeTaken() * MINUTE) > (System.currentTimeMillis() - 10 * MINUTE)) {
//                convertView.setBackgroundResource(R.color.overspeed_color);
//            } else {
//                convertView.setBackgroundColor(Color.TRANSPARENT);
//            }

        }
    }


    public Activity getmActivity() {
        return mActivity;
    }

    public void setmActivity(Activity mActivity) {
        this.mActivity = mActivity;
    }
}


