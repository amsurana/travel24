package com.smargav.travel24.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.j256.ormlite.dao.Dao;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.smargav.travel24.R;
import com.smargav.travel24.db.TransactionDBManager;
import com.smargav.travel24.db.TripDetailsDao;
import com.smargav.travel24.db.dbmodel.Checkpost;
import com.smargav.travel24.db.dbmodel.TripDetail;
import com.smargav.travel24.utils.APIUtils;
import com.smargav.travel24.utils.AppConstants;
import com.smargav.travel24.utils.PreferencesUtil;
import com.smargav.travel24.utils.Utils;

import java.util.ArrayList;
import java.util.List;


public class Entry extends Fragment implements AdapterView.OnItemSelectedListener, Validator.ValidationListener {


    @NotEmpty
    private EditText mVehicleNumber;

    @NotEmpty
    private EditText mDriverName;

    @NotEmpty
    private EditText mNumOfPass;

    private Button minusButton, plusButton;
    private Spinner mExitcp;
    //private Spinner entrySpinner;
    private int numberOfPassengers;
    private List<Checkpost> checkposts;

    private List<Long> cityIdList;

    private View rootView;
    private Spinner mVehicleType;
    private Validator validator;

    private Activity mActivity;

    public Entry() {
    }

    @Override
    public void onAttach(Activity activity) {
        mActivity = activity;
        super.onAttach(activity);
    }

    @Override
    public void onAttach(Context context) {
        mActivity = (Activity) context;
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_entry, container, false);
        numberOfPassengers = 0;

        mVehicleType = (Spinner) rootView.findViewById(R.id.vehicle_typeETID);
        mVehicleNumber = (EditText) rootView.findViewById(R.id.vehicle_numberETID);

        mNumOfPass = (EditText) rootView.findViewById(R.id.no_ofPassETID);
        mDriverName = (EditText) rootView.findViewById(R.id.driver_nameETID);
        //mEntryCheckPost = (Spinner) rootView.findViewById(R.id.EntryCheckPost);
        mExitcp = (Spinner) rootView.findViewById(R.id.exitCheckPost);
        minusButton = (Button) rootView.findViewById(R.id.minusBTNID);
        plusButton = (Button) rootView.findViewById(R.id.plusBTNID);

        final String value = String.valueOf(numberOfPassengers);
        mNumOfPass.setText(value);


        Button entrySubmit = (Button) rootView.findViewById(R.id.entrySubmitBTNID);
        minusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (numberOfPassengers > 0)
                    numberOfPassengers = numberOfPassengers - 1;
                String value = String.valueOf(numberOfPassengers);
                mNumOfPass.setText(value);

            }
        });

        plusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                numberOfPassengers = numberOfPassengers + 1;
                String value = String.valueOf(numberOfPassengers);
                mNumOfPass.setText(value);

            }
        });


        showListSpinner();


        entrySubmit.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                validator.validate();
            }
        });


        validator = new Validator(this);
        validator.setValidationListener(this);

        // attaching data adapter to spinner
        return rootView;
    }

    private void saveEntry() {
        try {
            TripDetailsDao dao = TransactionDBManager.getInstance().getEntryDetailsDAO();

            TripDetail details = new TripDetail();

            details.setSynced(false);
            details.setDriverName(mDriverName.getText().toString());
            details.setEndCp(checkposts.get(mExitcp.getSelectedItemPosition()).getId());
            details.setStartCp(PreferencesUtil.get(getActivity(), AppConstants.DEF_CP, Checkpost.class).getId());
            //details.setStartCp(checkposts.get(mEntryCheckPost.getSelectedItemPosition()).getId());
            details.setEntryTime(System.currentTimeMillis());
            details.setFromNet(false);
            details.setNumberOfPassenger(numberOfPassengers);
            details.setVehicleNo(mVehicleNumber.getText().toString());
            details.setVehicleType("" + mVehicleType.getSelectedItem());

            Dao.CreateOrUpdateStatus status = dao.createOrUpdate(details);
            Log.i("EntryFrag", "Saved? " + status.isCreated() + " or " + status.isUpdated());

            if (Utils.hasNetwork(getActivity())) {
                APIUtils.submit(details);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void showTimePickerDialog(View v) {

//        DialogFragment newFragment = new TimePickerFragment();
//        newFragment.show(getFragmentManager(), "timePicker");
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


    private void showListSpinner() {

        checkposts = PreferencesUtil.get(getActivity(), AppConstants.CHECKPOSTS, new TypeToken<List<Checkpost>>() {
        }.getType());

        List<String> items = new ArrayList<>();
        cityIdList = new ArrayList<>();


        for (int i = 0; i < checkposts.size(); i++) {
            items.add(checkposts.get(i).getName());
            Long idValue = checkposts.get(i).getId();
            cityIdList.add(idValue);
        }

        //ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, items);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, items);

        //mEntryCheckPost.setAdapter(adapter);
        mExitcp.setAdapter(adapter2);

    }

    @Override
    public void onValidationSucceeded() {
        Utils.showPrompt(getActivity(), "Confirm", "Save and submit entry?", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE) {
                    saveEntry();
                    Toast.makeText(getActivity(), "Entry saved successfully. ", Toast.LENGTH_LONG).show();
                    resetForm();
                }
            }
        }, new String[]{"OK", "Cancel"});
    }

    private void resetForm() {
        mVehicleNumber.setText("");
        mDriverName.setText("");
        mNumOfPass.setText("0");
        showListSpinner();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        Utils.showPrompt(getActivity(), "Form error", "Please enter correct values.");

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            }
        }
    }


    public Activity getmActivity() {
        return mActivity;
    }

    public void setmActivity(Activity mActivity) {
        this.mActivity = mActivity;
    }
}
