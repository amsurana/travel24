package com.smargav.travel24.bg;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.util.Log;

import com.j256.ormlite.dao.CloseableIterable;
import com.smargav.travel24.db.NetResponse;
import com.smargav.travel24.db.TransactionDBManager;
import com.smargav.travel24.db.TripDetailsDao;
import com.smargav.travel24.db.dbmodel.Checkpost;
import com.smargav.travel24.db.dbmodel.TripDetail;
import com.smargav.travel24.utils.APIUtils;
import com.smargav.travel24.utils.AppConstants;
import com.smargav.travel24.utils.PreferencesUtil;

import java.util.Iterator;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;


@SuppressWarnings("unchecked")
public class SyncService extends Service {
    public static final String SET_ALARM = "com.smargav.SyncService.SET_ALARM";
    public static String RUN_SYNC = "com.smargav.SyncService.RUN_SYNC";
    public static String RUN_SYNC_MANDATORY = "com.smargav.SyncService.RUN_SYNC_MANDATORY";
    public static final String SYNC_STATUS = "syncStatusKey";
    private SyncRunner runner;

    public static String TAG = "SyncService";
    //Run sync every 5 minutes
    long TIME_INTERVAL = 5 * 60 * 1000;

    @Override
    public void onCreate() {
        super.onCreate();
        TransactionDBManager.init(this);
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent != null && (SET_ALARM.equals(intent.getAction()))) {
            resetAlarm();
            return START_STICKY;
        }
        if (intent != null
                && (RUN_SYNC.equals(intent.getAction()) || RUN_SYNC_MANDATORY.equals(intent.getAction()))) {

            if (RUN_SYNC_MANDATORY.equals(intent.getAction())) {
                PreferencesUtil.putBool(this, SYNC_STATUS, false);
            }

            runSync();
            return START_STICKY;
        }

        long value = PreferencesUtil.getLong(this, ALARM_SERVICE, 0);
        if (!PreferencesUtil.contains(this, ALARM_SERVICE) || value != TIME_INTERVAL) {
            resetAlarm();
            PreferencesUtil.putLong(this, ALARM_SERVICE, TIME_INTERVAL);
        }

        runSync();
        return START_NOT_STICKY;
    }

    private void runSync() {
        runner = new SyncRunner();
        runner.start();
    }

    private void resetAlarm() {

        AlarmManager mgr = (AlarmManager) getSystemService(ALARM_SERVICE);
        Intent i = new Intent(this, SyncService.class);
        i.setAction(RUN_SYNC);
        PendingIntent pi = PendingIntent.getService(this, 0, i, 0);
        long TIME_INTERVAL = 60 * 1000;

        mgr.cancel(pi);
        mgr.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + TIME_INTERVAL,
                TIME_INTERVAL, pi);

        // run once every 1 hours..
        TIME_INTERVAL = AlarmManager.INTERVAL_HOUR;
        i = new Intent(this, SyncService.class);
        i.setAction(RUN_SYNC_MANDATORY);
        pi = PendingIntent.getService(this, 10, i, 0);
        mgr.cancel(pi);
        mgr.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + TIME_INTERVAL,
                TIME_INTERVAL, pi);
    }

    private class SyncRunner extends Thread {

        public void run() {
            boolean syncStatus = PreferencesUtil.getBool(SyncService.this, SYNC_STATUS, false);

            if (!PreferencesUtil.contains(SyncService.this, AppConstants.DEF_CP)) {
                Log.i(TAG, "App not setup yet. Will run later.");
                return;
            }
//            if (syncStatus) {
//                Log.i(TAG, "Sync already in progress. Will try again later.");
//                return;
//            }

            try {
                Log.i(TAG, "Starting Sync Service.");
                PreferencesUtil.putBool(SyncService.this, SYNC_STATUS, true);
                //Run sync only if we have network.
                if (hasNetwork()) {
                    sync();
                    loadExitVehicles();
                }
                purge();
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, e.getMessage(), e);
            }

            //Sync Over!
            PreferencesUtil.putBool(SyncService.this, SYNC_STATUS, false);


        }


    }

    /**
     * Remove policy is for all the data
     */
    private void purge() throws Exception {
        TripDetailsDao dao = TransactionDBManager.getInstance().getEntryDetailsDAO();
        dao.deleteOldRecords();
    }

    private void sync() throws Exception {
        CloseableIterable<TripDetail> it = null;
        try {
            TripDetailsDao dao = TransactionDBManager.getInstance().getEntryDetailsDAO();
            it = dao.getAllUnsynced();

            for (TripDetail tripDetail : it) {
                Log.i(TAG, "Trying to sync: " + tripDetail.getId());
                //TripDetail copy = tripDetail.copy();
                boolean isSent = sendToServer(tripDetail);
                if (isSent) {
                    tripDetail.setSynced(true);
                    //dao.update(tripDetail);
                    dao.delete(tripDetail);
                }
            }
        } finally {
            if (it != null) {
                it.closeableIterator();
            }
        }
    }

    private boolean sendToServer(TripDetail tripDetail) {

        try {
            return APIUtils.sync(tripDetail);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean hasNetwork() {
        ConnectivityManager manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo info = manager.getActiveNetworkInfo();
        return info != null && info.isConnected();
    }


    public static void startSync(Context context) {
        try {
            Intent intent = new Intent(context, SyncService.class);
            intent.setAction(SyncService.SET_ALARM);
            context.startService(intent);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage(), e);

        }

    }

    private void loadExitVehicles() {

        Call<NetResponse<List<TripDetail>>> callExit = APIUtils.search(PreferencesUtil.get(this, AppConstants.DEF_CP, Checkpost.class).getId());
        callExit.enqueue(new Callback<NetResponse<List<TripDetail>>>() {
            @Override
            public void onResponse(Response<NetResponse<List<TripDetail>>> response, Retrofit retrofit) {
                List<TripDetail> exitListView = response.body().getResp();
                saveList(exitListView);
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e("ExitFrag", t.getMessage(), t);
            }
        });

    }


    private void saveList(List<TripDetail> exitListView) {

        try {

            TripDetailsDao dao = TransactionDBManager.getInstance().getEntryDetailsDAO();

            Iterator<TripDetail> it = exitListView.iterator();
            while (it.hasNext()) {
                TripDetail detail = it.next();

                detail.setVehicleType(detail.getVehicleDetails().getType());
                detail.setVehicleNo(detail.getVehicleDetails().getNumber());

                TripDetail saved = dao.queryForSameId(detail);
                if (saved != null && saved.getExitTime() != 0) {
                    it.remove();
                    continue;
                }

                detail.setVehicleType(detail.getVehicleDetails().getType());
                detail.setVehicleNo(detail.getVehicleDetails().getNumber());
                //We set it to synced by default. We flip status once details are updated.
                detail.setSynced(true);
                detail.setFromNet(true);

                dao.createOrUpdate(detail);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
