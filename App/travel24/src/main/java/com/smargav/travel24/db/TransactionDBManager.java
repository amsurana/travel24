package com.smargav.travel24.db;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.smargav.travel24.db.dbmodel.Checkpost;
import com.smargav.travel24.db.dbmodel.TripDetail;

import java.sql.SQLException;

public class TransactionDBManager {
    static private TransactionDBManager instance;
    public static String dbName = "travel24.db";

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TransactionDBManager(ctx);
        }
    }

    static public TransactionDBManager getInstance() {
        return instance;
    }

    private DatabaseHelper helper;
    private Class[] dbClasses = {Checkpost.class, TripDetail.class};

    private TransactionDBManager(Context ctx) {
        helper = new DatabaseHelper(ctx, dbClasses, dbName);
    }

    public CheckpostDao getCheckpostDao() throws Exception {
        return (CheckpostDao) createDao(Checkpost.class);
    }

    public TripDetailsDao getEntryDetailsDAO() throws Exception {
        return (TripDetailsDao) createDao(TripDetail.class);
    }

    private <D> Dao<D, ?> createDao(Class<D> clazz) throws SQLException {
        return DaoManager.createDao(helper.getConnectionSource(), clazz);
    }

    private <D> Dao<D, ?> createDao(ConnectionSource connSrc, Class<D> clazz) throws SQLException {
        return DaoManager.createDao(connSrc, clazz);
    }

    public void clearTables() {
        for (Class tableClass : dbClasses) {
            try {
                TableUtils.clearTable(helper.getConnectionSource(), tableClass);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}