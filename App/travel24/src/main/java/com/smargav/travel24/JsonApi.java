package com.smargav.travel24;

import com.smargav.travel24.db.NetRequest;
import com.smargav.travel24.db.NetResponse;
import com.smargav.travel24.db.dbmodel.Checkpost;
import com.smargav.travel24.db.dbmodel.CpDistance;
import com.smargav.travel24.db.dbmodel.TripDetail;

import java.util.List;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by smargav on 28/8/16.
 */

public interface JsonApi {
    @POST("trip/add")
    public Call<NetResponse<String>> postEntry(@Body NetRequest request);

    //ListView in Exit Screen
    @GET("trip/byendcp")
    public Call<NetResponse<List<TripDetail>>> getResponse(@Query("id") long cp);

    @POST("trip/search")
    public Call<NetResponse<List<TripDetail>>> search(@Body NetRequest request);

    @GET("checkpost/all")
    public Call<NetResponse<List<Checkpost>>> getCheckposts();

    @GET("master/all")
    public Call<NetResponse<List<CpDistance>>> getCpDistance();
}


