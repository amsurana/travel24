package com.smargav.travel24.db.dbmodel;

import java.io.Serializable;
import java.math.BigInteger;


/**
 * The persistent class for the vehicles database table.
 */
public class Vehicle implements Serializable {
    private static final long serialVersionUID = 1L;

    private long id;

    private long createdTime;

    private String type;

    private String number;

    private BigInteger updatedTime;


    public Vehicle() {
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public long getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(long createdTime) {
        this.createdTime = createdTime;
    }


    public String getType() {
        return type;
    }


    public void setType(String type) {
        this.type = type;
    }


    public String getNumber() {
        return this.number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public BigInteger getUpdatedTime() {
        return this.updatedTime;
    }

    public void setUpdatedTime(BigInteger updatedTime) {
        this.updatedTime = updatedTime;
    }


}