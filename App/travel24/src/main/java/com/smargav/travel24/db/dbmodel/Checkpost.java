package com.smargav.travel24.db.dbmodel;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.smargav.travel24.db.CheckpostDao;

/**
 * Created by Amit S on 02/09/16.
 */
@DatabaseTable(daoClass = CheckpostDao.class)
public class Checkpost {

    @DatabaseField(id = true)
    private long id;

    @DatabaseField
    private long createdTime;

    @DatabaseField
    private String name;

    @DatabaseField
    private long updatedTime;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(long createdTime) {
        this.createdTime = createdTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(long updatedTime) {
        this.updatedTime = updatedTime;
    }
}
