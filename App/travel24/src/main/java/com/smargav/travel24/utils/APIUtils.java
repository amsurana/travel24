package com.smargav.travel24.utils;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.smargav.travel24.JsonApi;
import com.smargav.travel24.db.NetRequest;
import com.smargav.travel24.db.NetResponse;
import com.smargav.travel24.db.TransactionDBManager;
import com.smargav.travel24.db.TripDetailSearchReq;
import com.smargav.travel24.db.TripDetailsDao;
import com.smargav.travel24.db.dbmodel.Checkpost;
import com.smargav.travel24.db.dbmodel.CpDistance;
import com.smargav.travel24.db.dbmodel.TripDetail;

import org.joda.time.LocalDate;

import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by Amit S on 02/09/16.
 */
public class APIUtils {
    public static Retrofit adapter;
    public static JsonApi api;

    static {
        adapter = new Retrofit.Builder().baseUrl("http://54.169.210.217:8080/travel24/").addConverterFactory(GsonConverterFactory.create()).build();
        //adapter = new Retrofit.Builder().baseUrl("http://192.168.1.50:8080/travel24/").addConverterFactory(GsonConverterFactory.create()).build();
        api = adapter.create(JsonApi.class);
    }


    public static boolean submit(final TripDetail entryDetails) {
        try {
            NetRequest req = new NetRequest();
            req.setRequest(entryDetails);
            Call<NetResponse<String>> r = api.postEntry(req);

            r.enqueue(new Callback<NetResponse<String>>() {
                @Override
                public void onResponse(Response<NetResponse<String>> response, Retrofit retrofit) {
                    Log.i("APIUtils", "Response: " + new Gson().toJson(response));

                    if (response.body().getCode() == 200) {
                        try {
                            TripDetailsDao dao = TransactionDBManager.getInstance().getEntryDetailsDAO();
                           int deleted =  dao.delete(entryDetails);
                            Log.i("APIUtils", "Synced & Removed - " + deleted + entryDetails.getId());
//                            entryDetails.setSynced(true);
//                            entryDetails.setCompleted(true);
//                            dao.createOrUpdate(entryDetails);
                        } catch (Exception e) {
                                e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    Log.e("APIUtils", t.getMessage(), t);
                }
            });

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    public static boolean sync(final TripDetail entryDetails) {
        try {
            NetRequest req = new NetRequest();
            req.setRequest(entryDetails);
            Call<NetResponse<String>> r = api.postEntry(req);
            Response<NetResponse<String>> res = r.execute();
            return res.body().getCode() == 200;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    public static Call<NetResponse<List<TripDetail>>> search(long id) {
        NetRequest request = new NetRequest();
        TripDetailSearchReq r = new TripDetailSearchReq();
        r.setToCp(id);
        r.setFromDate(LocalDate.now().toDateTimeAtStartOfDay().getMillis());
        r.setToDate(LocalDate.now().plusDays(1).toDateTimeAtStartOfDay().getMillis());
        request.setRequest(r);
        Call<NetResponse<List<TripDetail>>> callExit = APIUtils.api.search(request);
        return callExit;
    }


    public static void downloadCpDistances(final Context context) {
        //Get All Cp Distances.

        try {
            Call<NetResponse<List<CpDistance>>> aCall = APIUtils.api.getCpDistance();

            aCall.enqueue(new Callback<NetResponse<List<CpDistance>>>() {
                @Override
                public void onResponse(Response<NetResponse<List<CpDistance>>> response, Retrofit retrofit) {
                    NetResponse<List<CpDistance>> body = response.body();
                    if (body.getResp() != null) {
                        Log.i("APIUtils", "Downloaded all CP Distances - " + body.getResp().size());
                        PreferencesUtil.put(context, AppConstants.CP_DISTANCES, body.getResp());
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    Log.e("APIUtils", t.getMessage(), t);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void downloadCheckposts(final Context context) {
        try {
            Call<NetResponse<List<Checkpost>>> call = APIUtils.api.getCheckposts();
            call.enqueue(new Callback<NetResponse<List<Checkpost>>>() {
                @Override
                public void onResponse(Response<NetResponse<List<Checkpost>>> response, Retrofit retrofit) {
                    NetResponse<List<Checkpost>> body = response.body();
                    List<Checkpost> checkposts = body.getResp();
                    if (checkposts != null && !checkposts.isEmpty()) {
                        PreferencesUtil.put(context, AppConstants.CHECKPOSTS, checkposts);
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    Log.e("APIUtils", t.getMessage(), t);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
