package com.smargav.travel24.db;

import android.util.Log;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.CloseableIterable;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.smargav.travel24.db.dbmodel.TripDetail;

import java.sql.SQLException;
import java.util.List;

@SuppressWarnings("unchecked")
public class TripDetailsDao extends BaseDaoImpl<TripDetail, String> {

    public TripDetailsDao(ConnectionSource connectionSource, Class<TripDetail> dataClass)
            throws SQLException {
        super(connectionSource, dataClass);
    }


    public CloseableIterable<TripDetail> getAllUnsynced() throws Exception {

        QueryBuilder<TripDetail, String> qb = queryBuilder();
        qb.where().eq("synced", false).and().eq("fromNet", false);
        PreparedQuery<TripDetail> query = qb.prepare();
        Log.i("TripDetailsDao", "Query: " + query.getStatement());
        return getWrappedIterable(query);
    }

    public void deleteOldRecords() throws SQLException {
        DeleteBuilder<TripDetail, String> qb = deleteBuilder();
        qb.where().eq("synced", true).and().eq("fromNet", false);
        delete(qb.prepare());
    }

    public List<TripDetail> getAllFromNet() throws Exception {
        QueryBuilder<TripDetail, String> qb = queryBuilder();
        //qb.where().eq("synced", true).and().eq("fromNet", true);
        qb.where().eq("fromNet", true).and().eq("completed", false).and().eq("exitTime", 0);
        PreparedQuery<TripDetail> query = qb.prepare();
        Log.i("TripDetailsDao", "Query: " + query.getStatement());
        return query(qb.prepare());

    }
}
