package com.smargav.travel24.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Bitmap;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.smargav.travel24.R;

import java.io.ByteArrayOutputStream;

public class Utils {


    public static boolean hasNetwork(Context ctx) {
        try {
            ConnectivityManager mgr = (ConnectivityManager) ctx
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo info = mgr.getActiveNetworkInfo();
            return info != null && info.isConnected();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }


    public static void showToast(Context ctx, String message) {
        Toast toast = Toast.makeText(ctx, message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if (v != null) {
            v.setGravity(Gravity.CENTER);
            v.setTextSize(22);
        }
        toast.show();
    }

    public static void showPrompt(Activity ctx, int title, int messageId) {
        showPrompt(ctx, ctx.getString(title), ctx.getString(messageId));
    }

    public static void showPrompt(Activity ctx, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        View customTitleView = ctx.getLayoutInflater().inflate(R.layout.dialog_title, null);
        builder.setCancelable(false);
        ((TextView) customTitleView.findViewById(R.id.dialog_title)).setText(title);
        builder.setCustomTitle(customTitleView).setMessage(message).setPositiveButton("OK", null);
        final Dialog dialog = builder.create();
        customTitleView.findViewById(R.id.dialog_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public static void showPrompt(final Activity ctx, int title, int message, final boolean finish) {
        showPrompt(ctx, ctx.getString(title), ctx.getString(message), finish);

    }

    public static void showPrompt(final Activity ctx, String title, String message, final boolean finish) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        View customTitleView = ctx.getLayoutInflater().inflate(R.layout.dialog_title, null);
        ((TextView) customTitleView.findViewById(R.id.dialog_title)).setText(title);
        builder.setCancelable(false);
        builder.setCustomTitle(customTitleView).setMessage(message)
                .setPositiveButton("OK", new OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (finish) {
                            ctx.setResult(Activity.RESULT_OK);
                            ctx.finish();
                        }
                    }
                });
        final Dialog dialog = builder.create();
        customTitleView.findViewById(R.id.dialog_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dialog.dismiss();
                if (finish) {
                    ctx.setResult(Activity.RESULT_OK);
                    ctx.finish();
                }
            }
        });
        dialog.show();
    }


    public static void showYesNoPrompt(final Activity ctx, int title, int message,
                                       OnClickListener listener) {
        showYesNoPrompt(ctx, ctx.getString(title), ctx.getString(message), listener);

    }

    public static void showYesNoPrompt(final Activity ctx, String title, String message,
                                       OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        View customTitleView = ctx.getLayoutInflater().inflate(R.layout.dialog_title, null);
        ((TextView) customTitleView.findViewById(R.id.dialog_title)).setText(title);
        builder.setCancelable(false);
        builder.setCustomTitle(customTitleView).setMessage(message).setPositiveButton("Yes", listener)
                .setNegativeButton("No", listener);
        final Dialog dialog = builder.create();
        customTitleView.findViewById(R.id.dialog_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public static void showPrompt(final Activity ctx, int title, int message,
                                  OnClickListener listener, int[] buttons) {
        showPrompt(ctx, ctx.getString(title), ctx.getString(message), listener, buttons);
    }

    public static void showPrompt(final Activity ctx, String title, String message,
                                  OnClickListener listener, int[] buttons) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        View customTitleView = ctx.getLayoutInflater().inflate(R.layout.dialog_title, null);
        TextView titleText = ((TextView) customTitleView.findViewById(R.id.dialog_title));
        titleText.setText(title);
        builder.setCustomTitle(customTitleView).setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(buttons[0], listener);
        if (buttons.length == 2) {
            builder.setNegativeButton(buttons[1], listener);
        }
        if (buttons.length == 3) {
            builder.setNeutralButton(buttons[2], listener);
        }

        final Dialog dialog = builder.create();
        customTitleView.findViewById(R.id.dialog_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    public static void showPrompt(final Activity ctx, String title, String message,
                                  OnClickListener listener, String[] buttons) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        View customTitleView = ctx.getLayoutInflater().inflate(R.layout.dialog_title, null);
        TextView titleText = ((TextView) customTitleView.findViewById(R.id.dialog_title));
        titleText.setText(title);
        builder.setCustomTitle(customTitleView).setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(buttons[0], listener);
        if (buttons.length == 2) {
            builder.setNegativeButton(buttons[1], listener);
        }
        if (buttons.length == 3) {
            builder.setNeutralButton(buttons[2], listener);
        }

        final Dialog dialog = builder.create();
        customTitleView.findViewById(R.id.dialog_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    public static void showNonCancelablePrompt(final Activity ctx, String title, String message,
                                               OnClickListener listener, int[] buttons) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        View customTitleView = ctx.getLayoutInflater().inflate(R.layout.dialog_title, null);
        TextView titleText = ((TextView) customTitleView.findViewById(R.id.dialog_title));
        titleText.setText(title);
        builder.setCustomTitle(customTitleView).setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(buttons[0], listener);
        if (buttons.length == 2) {
            builder.setNegativeButton(buttons[1], listener);
        }
        if (buttons.length == 3) {
            builder.setNeutralButton(buttons[2], listener);
        }

        final Dialog dialog = builder.create();
        customTitleView.findViewById(R.id.dialog_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //dialog.dismiss();
            }
        });
        dialog.show();
    }


    public static String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }

    public static String getNetworkClass(NetworkInfo info) {

        if (info.getType() == ConnectivityManager.TYPE_WIFI)
            return "WIFI";
        if (info.getType() == ConnectivityManager.TYPE_MOBILE) {
            int networkType = info.getSubtype();
            switch (networkType) {
                case TelephonyManager.NETWORK_TYPE_GPRS:
                case TelephonyManager.NETWORK_TYPE_EDGE:
                case TelephonyManager.NETWORK_TYPE_CDMA:
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                case TelephonyManager.NETWORK_TYPE_IDEN:
                    return "2G";
                case TelephonyManager.NETWORK_TYPE_UMTS:
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                case TelephonyManager.NETWORK_TYPE_HSPA:
                case TelephonyManager.NETWORK_TYPE_EVDO_B:
                case TelephonyManager.NETWORK_TYPE_EHRPD:
                case TelephonyManager.NETWORK_TYPE_HSPAP:
                    return "3G";
                case TelephonyManager.NETWORK_TYPE_LTE:
                    return "4G";
                default:
                    return "NA";
            }
        }
        return "NA";
    }

    public static boolean isLuhnFormat(String custUrn) {

        int[] digits = new int[custUrn.length()];

        for (int i = 0; i < custUrn.length(); i++) {
            digits[i] = Character.getNumericValue(custUrn.charAt(i));
        }

        int sum = 0;
        int length = digits.length;
        for (int i = 0; i < length; i++) {
            // get digits in reverse order
            int digit = digits[length - i - 1];
            // every 2nd number multiply with 2
            if (i % 2 == 1) {
                digit *= 2;
            }
            sum += digit > 9 ? digit - 9 : digit;
        }
        return sum % 10 == 0;
    }


    public static void playNotificationTone(Context ctx) {
        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(ctx, notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
