package com.smargav.travel24.db.dbmodel;

/**
 * The persistent class for the master_details database table.
 */
public class CpDistance {

    private long id;

    private long createdTime;

    private long distance;

    private long fromCp;

    private long toCp;

    private long timeTaken;

    private long updatedTime;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(long createdTime) {
        this.createdTime = createdTime;
    }

    public long getDistance() {
        return distance;
    }

    public void setDistance(long distance) {
        this.distance = distance;
    }

    public long getFromCp() {
        return fromCp;
    }

    public void setFromCp(long fromCp) {
        this.fromCp = fromCp;
    }

    public long getToCp() {
        return toCp;
    }

    public void setToCp(long toCp) {
        this.toCp = toCp;
    }

    public long getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(long timeTaken) {
        this.timeTaken = timeTaken;
    }

    public long getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(long updatedTime) {
        this.updatedTime = updatedTime;
    }
}