package com.smargav.travel24.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.j256.ormlite.dao.Dao;
import com.smargav.travel24.R;
import com.smargav.travel24.db.TransactionDBManager;
import com.smargav.travel24.db.TripDetailsDao;
import com.smargav.travel24.db.dbmodel.Checkpost;
import com.smargav.travel24.db.dbmodel.CpDistance;
import com.smargav.travel24.db.dbmodel.TripDetail;
import com.smargav.travel24.utils.APIUtils;
import com.smargav.travel24.utils.AppConstants;
import com.smargav.travel24.utils.PreferencesUtil;
import com.smargav.travel24.utils.Utils;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.List;
import java.util.Map;

/**
 * Created by Amit S on 03/09/16.
 */
public class TripCompletionDialog extends DialogFragment {

    private View rootView;
    private TripDetail tripDetail;
    private EditText remarks;
    private Button submit;
    private Spinner overDelay;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.trip_details, null);


        initView();
        return rootView;
    }

    private void initView() {
        ((TextView) rootView.findViewById(R.id.trip_cp)).setText(getCpName(tripDetail.getStartCp()));
        ((TextView) rootView.findViewById(R.id.trip_driver_name)).setText(tripDetail.getDriverName());
        ((TextView) rootView.findViewById(R.id.trip_no_pass)).setText("" + tripDetail.getNumberOfPassenger());
        ((TextView) rootView.findViewById(R.id.trip_startTime)).setText(formatDt(tripDetail.getEntryTime()));
        ((TextView) rootView.findViewById(R.id.trip_vehType)).setText(tripDetail.getVehicleType());
        ((TextView) rootView.findViewById(R.id.trip_vno)).setText(tripDetail.getVehicleNo());
        ((TextView) rootView.findViewById(R.id.trip_schedTime)).setText(fillSchedTime(tripDetail));


        remarks = (EditText) rootView.findViewById(R.id.remarksETID);
        submit = (Button) rootView.findViewById(R.id.entrySubmitBTNID);
        overDelay = (Spinner) rootView.findViewById(R.id.over_delay);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveEntry();
            }
        });

        rootView.findViewById(R.id.cancelButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private String fillSchedTime(TripDetail detail) {

        Map<Long, CpDistance> m = Exit.map.get(detail.getStartCp());
        if (m == null) {
            return "NA";
        }

        CpDistance distance = m.get(detail.getEndCp());

        if (distance == null) {
            return "NA";
        }

        return formatDt(detail.getEntryTime() + distance.getTimeTaken() * 60 * 1000);
    }

    private void saveEntry() {
        try {
            TripDetailsDao dao = TransactionDBManager.getInstance().getEntryDetailsDAO();

            TripDetail details = tripDetail;

            details.setSynced(false);
            details.setFromNet(false);
            details.setRemark(remarks.getText().toString());
            details.setOver(overDelay.getSelectedItemPosition() == 1 ? 1 : 0);
            details.setDelay(overDelay.getSelectedItemPosition() == 2 ? 1 : 0);
            details.setExitTime(System.currentTimeMillis());
            details.setCompleted(true);

            Dao.CreateOrUpdateStatus status = dao.createOrUpdate(details);
            Log.i("EntryFrag", "Saved? " + status.isCreated() + " or " + status.isUpdated());

            if (Utils.hasNetwork(getActivity())) {
                APIUtils.submit(details);
            }

            Toast.makeText(getActivity(), "Saved information successfully. It will be submitted in background", Toast.LENGTH_SHORT).show();
            dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private String formatDt(long entryTime) {
        DateTimeFormatter format = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm");
        return format.print(entryTime);
    }

    private String getCpName(long startCp) {
        List<Checkpost> checkpostList = PreferencesUtil.get(getActivity(), AppConstants.CHECKPOSTS, new TypeToken<List<Checkpost>>() {
        }.getType());
        for (Checkpost cp : checkpostList) {
            if (cp.getId() == startCp) {
                return cp.getName();
            }
        }
        return "NA";
    }

    public void setTripDetail(TripDetail tripDetail) {
        this.tripDetail = tripDetail;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // layout to display
        //dialog.setContentView(R.layout.about_program_dialog_layout);

        // set color transpartent
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }
    }
}
