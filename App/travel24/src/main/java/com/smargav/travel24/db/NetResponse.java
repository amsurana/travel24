package com.smargav.travel24.db;

/**
 * Created by amu on 28/05/15.
 */
public class NetResponse<T> {

    private int code;
    private String message;
    private T resp;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getResp() {
        return resp;
    }

    public void setResp(T resp) {
        this.resp = resp;
    }
}
